//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "Sprite.hpp"
#include <cassert>
#include <cmath>
#include <limits>
#include <boost/foreach.hpp>
#include "Context.hpp"
#include "Utils.hpp"

using namespace benzaiten;

Sprite::Sprite (double x, double y, const Graphic::Ptr &graphic)
    :
    // protected
    context (0)
    , graphic (graphic)
    , type (-1)
    // private
    , active (true)
    , advance_rest (0.0, 0.0)
    , layer (0)
    , hitbox (makeRect (0, 0, 0, 0))
    , position (x, y)
    , render_next (0)
    , render_prev (0)
    , type_next (0)
    , type_prev (0)
    , update_next ()
    , update_prev (0)
{
}

void Sprite::advance (double x, double y, const TypeList &solid) {
    // Force to advance only in integer increments, but keeping the
    // rest of the advance for the next call to advance.
    this->advance_rest.x += x;
    this->advance_rest.y += y;
    x = roundf (this->advance_rest.x);
    y = roundf (this->advance_rest.y);
    this->advance_rest.x -= x;
    this->advance_rest.y -= y;

    if (solid.empty ()) {
        this->position.x += x;
        this->position.y += y;
    } else {
        if (x != 0) {
            if (this->collides (this->x () + x, this->y (), solid) != 0) {
                double dir = x > 0.0 ? 1.0 : -1.0;
                while ( x != 0) {
                    Sprite *sprite =
                        this->collides (this->x () + dir, this->y (), solid);
                    if (sprite != 0) {
                        this->advanceCollideX (*sprite);
                        break;
                    } else {
                        this->position.x += dir;
                        x -= dir;
                    }
                }
            } else {
                this->position.x += x;
            }
        }
        if (y != 0) {
            if (this->collides (this->x (), this->y () + y, solid) != 0) {
                double dir = y > 0.0 ? 1.0 : -1.0;
                while (y != 0) {
                    Sprite *sprite =
                        this->collides (this->x (), this->y () + dir, solid);
                    if (sprite != 0) {
                        this->advanceCollideY (*sprite);
                        break;
                    } else {
                        this->position.y += dir;
                        y -= dir;
                    }
                }
            } else {
                this->position.y += y;
            }
        }
    }
}

bool Sprite::collides (double x, double y, const Point2D<double> &point) const {
    SDL_Rect rect = makeRect (
            x - this->hitbox.x, y - this->hitbox.y,
            this->hitbox.w, this->hitbox.h);
    return point.x >= rect.x && point.x <= (rect.x + rect.w) &&
        point.y >= rect.y && point.y <= (rect.y + rect.h);
}

bool Sprite::collides (double x, double y, const Sprite &other) const {
    SDL_Rect this_rect = makeRect (
            x - this->hitbox.x, y - this->hitbox.y,
            this->hitbox.w, this->hitbox.h);
    SDL_Rect other_rect = makeRect (
            other.x () - other.hitbox.x, other.y () - other.hitbox.y,
            other.hitbox.w, other.hitbox.h);
    return SDL_HasIntersection (&this_rect, &other_rect);
}

Sprite *Sprite::collides (double x, double y, int type) const {
    if (this->context != 0) {
        for (Sprite *sprite = this->context->firstOfType (type) ;
                sprite != 0 ; sprite = sprite->type_next) {
            if (this != sprite && this->collides (x, y, *sprite)) {
                return sprite;
            }
        }
    }
    return 0;
}

Sprite *Sprite::collides (double x, double y, const TypeList &types) const {
    if (this->context != 0 && !types.empty ()) {
        BOOST_FOREACH (int type, types) {
            for (Sprite *sprite = this->context->firstOfType (type) ;
                    sprite != 0 ; sprite = sprite->type_next) {
                if (this != sprite && this->collides (x, y, *sprite)) {
                    return sprite;
                }
            }
        }
    }
    return 0;
}

double Sprite::distanceSquaredTo (const Sprite::Ptr &other) const {
    if (other == 0) {
        return std::numeric_limits<double>::max ();
    }
    return this->distanceSquaredTo (*other);
}

double Sprite::distanceSquaredTo (const Sprite &other) const {
    return Point2D<double> (
            this->position.x - this->hitbox.x,
            this->position.y - this->hitbox.y).
        distanceSquaredTo (other.position);
}

void Sprite::moveTo (double x, double y) {
    this->position.x = x;
    this->position.y = y;
}

void Sprite::render (SDL_Renderer &renderer) const
{
    if (this->graphic && this->graphic->isVisible ()) {
        this->graphic->render (renderer, this->position.x, this->position.y);
    }
}

void Sprite::setGraphic (const Graphic::Ptr &graphic) {
    this->graphic = graphic;
}

void Sprite::setHitbox (int x, int y, int width, int height) {
    this->hitbox = makeRect (x, y, width, height);
}

void Sprite::setLayer (int layer) {
    if (this->layer != layer) {
        if (this->context == 0) {
            this->layer = layer;
        } else {
            this->context->removeRender (this);
            this->layer = layer;
            this->context->addRender (this);
        }
    }
}
