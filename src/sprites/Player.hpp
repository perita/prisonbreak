//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_PRISON_BREAK_SPRITE_PLAYER_HPP)
#define GEISHA_STUDIOS_PRISON_BREAK_SPRITE_PLAYER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace prisonbreak {
    class Player: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Player> Ptr;

            static Ptr New () { return Ptr (new Player ()); }

            void setActive (bool active) {
                this->active = active;
                this->graphic->setActive (active);
            }

            void useController (bool use) {
                this->use_controller = use;
            }

            void useMouse (bool use) {
                this->use_mouse = use;
            }

            virtual void update (double elapsed);

        protected:
            Player ();

        private:
            void checkMouse ();

            bool active;
            int mouse_dir_x;
            int mouse_dir_y;
            bool use_controller;
            bool use_mouse;
    };
}

#endif // !GEISHA_STUDIOS_PRISON_BREAK_SPRITE_PLAYER_HPP
