//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "FrameRateManager.hpp"
#include <algorithm>
#include <SDL_timer.h>

using namespace benzaiten;

namespace {
    /// The maximum allowed elapsed time.
    const double MAX_ELAPSED_TIME = 30.0 / 1000.0;

    ///
    /// @brief Gets the number of milliseconds since the program's start.
    ///
    /// @return The milliseconds since the program has started.
    ///
    uint32_t getCurrentTime () {
        return SDL_GetTicks ();
    }
}

FrameRateManager::FrameRateManager (double frameRate):
    elapsedTime (0.0),
    frameRate (static_cast<uint32_t> (1000.0 / frameRate)),
    lastTime (getCurrentTime ())
{
}

double FrameRateManager::getElapsedTime () const {
    return this->elapsedTime;
}

void FrameRateManager::update () {
    uint32_t time = getCurrentTime ();
    uint32_t elapsed = time - this->lastTime;
    this->elapsedTime = std::min (elapsed / 1000.0, MAX_ELAPSED_TIME);
    this->lastTime = time;
#if !defined(EMSCRIPTEN)
    if (this->frameRate > elapsed) {
        SDL_Delay (this->frameRate - elapsed);
    }
#endif // !EMSCRIPTEN
}
