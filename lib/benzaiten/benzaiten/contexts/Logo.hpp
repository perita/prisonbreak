//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <boost/shared_ptr.hpp>
#include "../Context.hpp"

namespace benzaiten { namespace contexts {

    ///
    /// @class Logo
    /// @brief Shows the logo a brief amount of time.
    ///
    class Logo: public Context
    {
        public:
            /// The pointer to a Logo instance.
            typedef boost::shared_ptr<Logo> Ptr;

            ///
            /// @brief Creates a new instance of the Logo context.
            ///
            /// @param[in] logo_file The file name of the logo to show.
            /// @param[in] next The next context to switch to when done.
            /// @param[in] display_time The time, in seconds, that the logo
            ///            shouldi be visible.
            /// @param[in] fade_time The time, in seconds, to fade in and
            ///            fade out.
            ///
            static Ptr New (const std::string &logo,
                    const Context::Ptr &next, double display_time = 1.0,
                    double fade_time = 0.75) {
                return Ptr (new Logo (logo, next, display_time, fade_time));
            }

            virtual void render (SDL_Renderer &renderer) const;
            virtual void update (double elapsed);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] logo_file The file name of the logo to show.
            /// @param[in] next The next context to switch to when done.
            /// @param[in] display_time The time, in seconds, that the logo
            ///            shouldi be visible.
            /// @param[in] fade_time The time, in seconds, to fade in and
            ///            fade out.
            ///
            Logo (const std::string &logo, const Context::Ptr &next,
                  double display_time = 1.0, double fade_time = 0.75);

        private:
            /// The alpha value for the fade.
            double alpha;
            /// The time that the logo should be visible.
            double display_time;
            /// The current time for the fade in.
            double fade_in;
            /// The current time for the fade out.
            double fade_out;
            /// The time fade in and out lasts.
            double fade_time;
            /// The next context to switch to.
            Context::Ptr next;
    };

} }

#endif // !GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP
