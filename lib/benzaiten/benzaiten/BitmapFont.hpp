//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FONT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FONT_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <map>
#include <string>
#include <SDL_rect.h>
#include <SDL_render.h>
#include <SDL_surface.h>
#include "SDL.hpp"


namespace benzaiten {

    ///
    /// @class BitmapFont
    /// @brief A bitmap based font.
    ///
    class BitmapFont {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] characters The list characters available in
            ///            @p surface.
            /// @param[in] surface The surface where the font's bitmaps are.
            ///
            BitmapFont (const std::string &characters, SDL::Surface surface);

            ///
            /// @brief Renders a string to a texture.
            ///
            /// @param[in] text The text to write.
            ///
            /// @return The texture with the text rendered.
            ///
            SDL::Texture renderText (const std::string &text) const;

            ///
            /// @brief Gets the font's height.
            ///
            /// @return The font's height.
            ///
            int height () const;

            ///
            /// @brief Gets the width that a text will have.
            ///
            /// @param[in] text The text to get its width.
            /// @return The width, in pixels, that @p text will have.
            ///
            int width (const std::string &text) const;

        private:
            /// The map of characters and rectangles.
            std::map<std::string::value_type, SDL_Rect> characters;
            /// The font's surface.
            SDL::Surface surface;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FONT_HPP
