//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "InputManager.hpp"
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

using namespace benzaiten;

namespace {
    const int SDL_X_AXIS = 0;
    const int SDL_Y_AXIS = 1;
    const int DEAD_ZONE = 5000;
}

InputManager::InputManager ()
    : mouse_down (false)
    , mouse_pressed (false)
    , mouse_released (false)
    , mouse_x (0)
    , mouse_y (0)
    , actions_state ()
    , axis_map ()
    , axis_x (AxisNone)
    , axis_y (AxisNone)
    , button_map ()
    , joystick (SDL_JoystickOpen (0))
    , key_map ()
{
    // Default mappings.
    using boost::assign::list_of;

    this->mapKeys (Action::MENU_ACCEPT, list_of<int> (SDLK_RETURN)(SDLK_z));
    this->mapKeys (Action::MENU_CANCEL, list_of<int> (SDLK_ESCAPE));
    this->mapKeys (Action::MENU_DOWN, list_of<int> (SDLK_DOWN));
    this->mapKeys (Action::MENU_LEFT, list_of<int> (SDLK_RIGHT));
    this->mapKeys (Action::MENU_RIGHT, list_of<int> (SDLK_DOWN));
    this->mapKeys (Action::MENU_UP, list_of<int> (SDLK_UP));

#if defined (ANDROID)
    this->mapKeys (Action::MENU_CANCEL, list_of<int> (SDLK_AC_BACK));
#endif // ANDROID

#if defined (GP2X)
    this->mapButtons (
            Action::MENU_ACCEPT, list_of (GP2X_BUTTON_A)(GP2X_BUTTON_B));
    this->mapButtons (Action::MENU_CANCEL, list_of (GP2X_BUTTON_X));
    this->mapButtons (Action::MENU_DOWN, list_of (GP2X_BUTTON_DOWN));
    this->mapButtons (Action::MENU_LEFT, list_of (GP2X_BUTTON_LEFT));
    this->mapButtons (Action::MENU_RIGHT, list_of (GP2X_BUTTON_RIGHT));
    this->mapButtons (Action::MENU_UP, list_of (GP2X_BUTTON_UP));
#else // !GP2X
    this->mapButtons (Action::MENU_ACCEPT, list_of (0)(7));
    this->mapButtons (Action::MENU_CANCEL, list_of (3)(6));
    this->mapAxisX (Action::MENU_LEFT, Action::MENU_RIGHT);
    this->mapAxisY (Action::MENU_UP, Action::MENU_DOWN);
#endif // GP2X
}

InputManager::~InputManager () {
    /* XXX: Somehow it makes the application crash in GP2X.
    if ( SDL_JoystickOpened (0) ) {
        SDL_JoystickClose (this->joystick);
    }
    */
}

void InputManager::handleAxis (const SDL_JoyAxisEvent &event) {
    if (SDL_X_AXIS == event.axis) {
        if (event.value > DEAD_ZONE) {
#if defined (ANDROID)
            setAxisX (AxisLeft);
#else // !ANDROID
            setAxisX (AxisRight);
#endif // ANDROID
        } else if (event.value < -DEAD_ZONE) {
#if defined (ANDROID)
            setAxisX (AxisRight);
#else // !ANDROID
            setAxisX (AxisLeft);
#endif // ANDROID
        } else {
            setAxisX (AxisNone);
        }
    } else if (SDL_Y_AXIS == event.axis) {
        if (event.value > DEAD_ZONE) {
            setAxisY (AxisDown);
        } else if (event.value < -DEAD_ZONE) {
            setAxisY (AxisUp);
        } else {
            setAxisY (AxisNone);
        }
    }
}

void InputManager::handleButton (const SDL_JoyButtonEvent &event) {
    bool button_down = event.type == SDL_JOYBUTTONDOWN;
    if (button_down) {
        this->actions_state[Action::ANY].pressed = true;
    } else {
        this->actions_state[Action::ANY].released = true;
    }

    ButtonMap::iterator button (this->button_map.find (event.button));
    if (button != this->button_map.end ()) {
        BOOST_FOREACH (int action, button->second) {
            this->actions_state[action].down = button_down;
            if (button_down) {
                this->actions_state[action].pressed = true;
            } else {
                this->actions_state[action].released = true;
            }
        }
    }
}

void InputManager::handleKeyboard (const SDL_KeyboardEvent &event) {
    bool key_down = event.type == SDL_KEYDOWN;
    if (key_down) {
        this->actions_state[Action::ANY].pressed = true;
    } else {
        this->actions_state[Action::ANY].released = true;
    }

    KeyMap::iterator key (this->key_map.find (event.keysym.sym));
    if (key != this->key_map.end ()) {
        BOOST_FOREACH (int action, key->second) {
            this->actions_state[action].down = key_down;
            if (key_down) {
                this->actions_state[action].pressed = true;
            } else {
                this->actions_state[action].released = true;
            }
        }
    }
}

bool InputManager::isDown (int action) const {
    StateMap::const_iterator state = this->actions_state.find (action);
    if (state != this->actions_state.end ()) {
        return state->second.down;
    }
    return false;
}

void InputManager::mapButtons (int action,
        const std::vector<Uint8> &buttons) {
    this->actions_state[action] = State ();
    BOOST_FOREACH (Uint8 button, buttons) {
        this->button_map[button].push_back (action);
    }
}

void InputManager::mapAxisX (int left_action, int right_action) {
    this->actions_state[left_action] = State ();
    this->axis_map[AxisLeft].push_back (left_action);

    this->actions_state[right_action] = State ();
    this->axis_map[AxisRight].push_back (right_action);
}

void InputManager::mapAxisY (int up_action, int down_action) {
    this->actions_state[up_action] = State ();
    this->axis_map[AxisUp].push_back (up_action);

    this->actions_state[down_action] = State ();
    this->axis_map[AxisDown].push_back (down_action);
}

void InputManager::mapKeys (int action,
        const std::vector<SDL_Keycode> &keys) {
    this->actions_state[action] = State ();
    BOOST_FOREACH (SDL_Keycode key, keys) {
        this->key_map[key].push_back (action);
    }
}

void InputManager::setAxisX (Axis axis) {
    if (this->axis_x != axis) {
        if (AxisNone != this->axis_x) {
            this->actions_state[Action::ANY].released = true;
            AxisMap::iterator actions (this->axis_map.find (this->axis_x));
            if (actions != this->axis_map.end()) {
                BOOST_FOREACH (int action, actions->second) {
                    this->actions_state[action].down = false;
                    this->actions_state[action].released = true;
                }
            }
        }
        if (AxisNone != axis) {
            this->actions_state[Action::ANY].pressed = true;
            AxisMap::iterator actions (this->axis_map.find (axis));
            if (actions != this->axis_map.end()) {
                BOOST_FOREACH (int action, actions->second) {
                    this->actions_state[action].down = true;
                    this->actions_state[action].pressed = true;
                }
            }
        }
        this->axis_x = axis;
    }
}

void InputManager::setAxisY (Axis axis) {
    if (this->axis_y != axis) {
        if (AxisNone != this->axis_y) {
            this->actions_state[Action::ANY].released = true;
            AxisMap::iterator actions (this->axis_map.find (this->axis_y));
            if (actions != this->axis_map.end()) {
                BOOST_FOREACH (int action, actions->second) {
                    this->actions_state[action].down = false;
                    this->actions_state[action].released = true;
                }
            }
        }
        if (AxisNone != axis) {
            this->actions_state[Action::ANY].pressed = true;
            AxisMap::iterator actions (this->axis_map.find (axis));
            if (actions != this->axis_map.end()) {
                BOOST_FOREACH (int action, actions->second) {
                    this->actions_state[action].down = true;
                    this->actions_state[action].pressed = true;
                }
            }
        }
        this->axis_y = axis;
    }
}

bool InputManager::pressed (int action) const {
    StateMap::const_iterator state = this->actions_state.find (action);
    if (state != this->actions_state.end ()) {
        return state->second.pressed;
    }
    return false;
}

bool InputManager::released (int action) const {
    StateMap::const_iterator state = this->actions_state.find (action);
    if (state != this->actions_state.end ()) {
        return state->second.released;
    }
    return false;
}

void InputManager::update () {
    this->mouse_pressed = false;
    this->mouse_released = false;
    BOOST_FOREACH (StateMap::value_type &action, this->actions_state) {
        action.second.pressed = false;
        action.second.released = false;
    }
}
