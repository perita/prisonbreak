//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Volume.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>

using namespace prisonbreak;

Volume::Volume (int x, int y, bool mute)
    : Sprite (x, y)
    , mute (mute)
{
    using benzaiten::Game;
    using benzaiten::TextureAtlas;

    this->setActive (false);
    TextureAtlas::Ptr atlas = Game::resources ().atlas ("volume.xml");
    atlas->play (mute ? "mute" : "active");
    atlas->centerOrigin ();

    this->setGraphic (atlas);
    this->setHitbox (12, 12, 24, 24);
}

void Volume::toggle () {
    using benzaiten::TextureAtlas;

    this->mute = !this->mute;
    reinterpret_cast<TextureAtlas &>(*(this->graphic)).play (
            this->mute ? "mute" : "active");
}
