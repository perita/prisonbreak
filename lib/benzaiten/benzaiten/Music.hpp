//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_MUSIC_HPP)
#define GEISHA_STUDIOS_BENZAITEN_MUSIC_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>

namespace benzaiten {
    ///
    /// @class Music
    /// @brief A music.
    ///
    class Music {
        public:
            /// A pointer to a music.
            typedef boost::shared_ptr<Music> Ptr;

            /// An infinite number of times to play.
            enum {
                Infinite = -1
            };

            ///
            /// @brief Destructor.
            ///
            virtual ~Music() { }

            ///
            /// @brief Halts the music.
            ///
            virtual void halt() = 0;

            ///
            /// @brief Pauses the music.
            ///
            virtual void pause() = 0;

            ///
            /// @brief Plays the music the specified number of times.
            ///
            /// @param[in] times The number of times to play the music.
            ///            \c Infinite is an special constant that
            ///            will play the music until stopped.
            ///
            virtual void play (int times = Infinite) = 0;

            ///
            /// @brief Resumes the music, if paused.
            ///
            virtual void resume() = 0;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_MUSIC_HPP
