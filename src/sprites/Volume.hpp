//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_PRISON_BREAK_SPRITE_VOLUME_HPP)
#define GEISHA_STUDIOS_PRISON_BREAK_SPRITE_VOLUME_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace prisonbreak {
    class Volume: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Volume> Ptr;

            static Ptr New (int x, int y, bool mute) {
                return Ptr (new Volume (x, y, mute));
            }

            bool isMute () const { return this->mute; }
            void setVisible (bool visible);
            void toggle ();

        protected:
            Volume (int x, int y, bool mute);

        private:
            bool mute;
    };
}

#endif // !GEISHA_STUDIOS_PRISON_BREAK_SPRITE_VOLUME_HPP
