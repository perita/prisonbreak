#!/bin/sh
set -ex

rm -fr build/wasm
mkdir -p build/wasm
cd build/wasm

mkdir src
cp -a ../../assets ../../src/index.html src
embuilder build boost_headers sdl2 sdl2_mixer sdl2_ttf
emcmake cmake -DCMAKE_CXX_FLAGS='-std=c++11 -sNO_DISABLE_EXCEPTION_CATCHING -sUSE_BOOST_HEADERS -sUSE_SDL=2 -sUSE_SDL_MIXER=2' -DCMAKE_EXE_LINKER_FLAGS='--preload-file assets' ../..
emmake make -j5
