//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Ball.hpp"
#include <vector>
#include <benzaiten/Game.hpp>
#include <benzaiten/Primitives.hpp>
#include <benzaiten/Screen.hpp>

using benzaiten::Game;

namespace {
    // The speed in pixels/second that the Ball moves around the screen.
    const double SPEED = 75.0;
}

Ball::Ball (const SDL_Color &color):
    // Create the ball at a random position within the screen.
    Sprite (rand () % Game::screen().width, rand() % Game::screen().height),
    color (color),
    dir_x ((rand () % 2) * 2 - 1), // A random direction for each axis.
    dir_y ((rand () % 2) * 2 - 1)
{
    // Sets the type of this Sprite to find it later using
    // Context::firstOfType ().  Type::BALL is defined in
    // config.h.cmake, but can be any integer.
    this->type = Type::BALL;
}

void Ball::render (SDL_Renderer &renderer) const {
    using benzaiten::renderFillCircle;

    // Render the Ball as a filled circle.
    renderFillCircle (renderer, this->x (), this->y (), this->getRadius (),
            this->color);
}

void Ball::update (double elapsed) {
    // Advance the ball in the current direction.  Then check whether it
    // reached any of the four border and invert the direction so the next
    // time update() is called the Ball will advance to the opposite
    // direction on that axis.
    this->advance (
            this->dir_x * SPEED * elapsed,
            this->dir_y * SPEED * elapsed);
    if ((this->dir_x < 0 && this->x () <= 0) ||
            (this->dir_x > 0 && this->x () >= Game::screen ().width)) {
        this->dir_x *= -1;
    }
    if ((this->dir_y < 0 && this->y () <= 0) ||
            (this->dir_y > 0 && this->y () >= Game::screen ().height)) {
        this->dir_y *= -1;
    }
}
