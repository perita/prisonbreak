//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Player.hpp"
#include <cmath>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/assign.hpp>

using namespace prisonbreak;

Player::Player ()
    : Sprite ()
    , active (true)
    , mouse_dir_x (0)
    , mouse_dir_y (0)
    , use_controller (true)
    , use_mouse (true)
{
    using benzaiten::Game;
    using benzaiten::TextureAtlas;
    using boost::assign::list_of;

    this->type = Types::PLAYER;

    TextureAtlas::Ptr atlas = Game::resources ().atlas ("player.xml");
    atlas->play ("run");
    atlas->centerOrigin ();

    this->setGraphic (atlas);
}

void Player::checkMouse () {
    using namespace benzaiten;

    if (this->use_mouse && Game::input ().mouse_down)  {
        int x = Game::input ().mouse_x;
        int y = Game::input ().mouse_y;
        this->mouse_dir_x = x - this->x ();
        if (this->mouse_dir_x != 0) {
            this->mouse_dir_x /= std::abs (this->mouse_dir_x);
        }
        this->mouse_dir_y = y - this->y ();
        if (this->mouse_dir_y != 0) {
            this->mouse_dir_y /= std::abs (this->mouse_dir_y);
        }
    } else {
        this->mouse_dir_x = 0;
        this->mouse_dir_y = 0;
    }
}

void Player::update (double elapsed) {
    using namespace benzaiten;

    if (this->active) {
        this->checkMouse ();
        int dir_y = this->mouse_dir_y;
        int dir_x = this->mouse_dir_x;

        if (this->use_controller) {
            if (Game::input ().isDown (Actions::DOWN)) {
                dir_y += 1;
            }
            if (Game::input ().isDown (Actions::UP)) {
                dir_y -= 1;
            }
            if (Game::input ().isDown (Actions::LEFT)) {
                dir_x -= 1;
            }
            if (Game::input ().isDown (Actions::RIGHT)) {
                dir_x += 1;
            }
        }

        double speed = 120.0 * elapsed;
        //if (dir_x != 0 && dir_y != 0) {
        //    // In diagonal, we need to go slower.
        //    // Around sin(45) = cos(45) = 0.7071, actually.
        //    speed *= 0.7071;
        //}
        this->advance (dir_x * speed, dir_y * speed);
        this->x (clamp (this->x (), 0.0, double (Game::screen().width)));
        this->y (clamp (this->y (), 0.0, double (Game::screen().height)));
    }
}
