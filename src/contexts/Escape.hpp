//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_PRISON_BREAK_CONTEXTS_ESCAPE_HPP)
#define GEISHA_STUDIOS_PRISON_BREAK_CONTEXTS_ESCAPE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

//#include <Sound.hpp>

#include <benzaiten/BitmapFont.hpp>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/Sound.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include "../sprites/Player.hpp"
#include "../sprites/Volume.hpp"

namespace prisonbreak {
    ///
    /// @class Escape
    /// @brief The main game's context.
    ///
    class Escape: public benzaiten::Context {
        public:
            /// A pointer to a Escape context instance.
            typedef boost::shared_ptr<Escape> Ptr;

            ///
            /// @brief Creates a new Escape context.
            ///
            /// @param[in] font The font to use to draw the texts.
            ///
            /// @return The pointer to the new Escape context.
            ///
            static Ptr New (const benzaiten::BitmapFont &font) {
                return Ptr (new Escape (font));
            }

            virtual void update (double elapsed);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] font The font to use to draw the texts.
            ///
            Escape (const benzaiten::BitmapFont &font);

        private:
            ///
            /// @brief Resets the game.
            ///
            void reset ();

            ///
            /// @brief Stops the game.
            ///
            void stop ();

            ///
            /// @brief Toggles the volume.
            ///
            void toggleVolume ();

            benzaiten::Sound::Ptr alarm;
            /// The background's grid.
            benzaiten::Graphic::Ptr grid;
            /// The escaped meters.
            double meters;
            /// The text with the escaped meters.
            benzaiten::Text::Ptr meters_text;
            /// The background music.
            benzaiten::Music::Ptr music;
            /// The player's sprite.
            Player::Ptr player;
            /// The random number generator. XXX: Must come before the
            /// variate generator, as the initialization
            /// list creates the objects in the given order here.
            boost::mt19937 random_generator;
            /// Compute the new direction of the guard.
            boost::variate_generator<boost::mt19937&, boost::uniform_int<> > random_direction;
            /// Compute the new position of the guard.
            boost::variate_generator<boost::mt19937&, boost::uniform_int<> > random_position;
            /// Compute the probability of a new guard.
            boost::variate_generator<boost::mt19937&, boost::uniform_real<> > random_probability;
            /// Compute the new speed of the guard.
            boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > random_speed;
            /// Tells whether to reset the music.
            bool reset_music;
            /// The probability of spawning a new guard.
            double spawn_probability;
            /// The text that tells the user to start.
            benzaiten::Text::Ptr start_text;
            /// The volume icon
            Volume::Ptr volume;
            /// Tells wheter to wait for start.
            bool wait_start;

#if defined (ANDROID)
            /// The left arrow.
            benzaiten::Sprite::Ptr arrow_left;
            /// The right arrow.
            benzaiten::Sprite::Ptr arrow_right;
            /// The text that shows the current control scheme.
            benzaiten::Text::Ptr input_text;
            /// Whether to use accelerator or mouse.
            bool use_accelerator;
#endif // ANDROID
    };
}

#endif // !GEISHA_STUDIOS_PRISON_BREAK_CONTEXTS_ESCAPE_HPP
