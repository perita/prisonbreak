//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "TileMap.hpp"
#include <cassert>
#include <boost/foreach.hpp>
#include "../Game.hpp"
#include "../Utils.hpp"

using namespace benzaiten;

TileMap::TileMap (const SDL::Texture &tiles, unsigned int columns,
        unsigned int rows, int tile_width, int tile_height)
    : Graphic ()
    , map (boost::extents[rows][columns])
    , tile (makeRect (0, 0, tile_width, tile_height))
    , tiles (tiles)
    , tiles_cols (0)
    , tiles_rows (0)
{
    assert (columns > 0 && "The columns can't be 0");
    assert (rows > 0 && "The rows can't be 0");
    assert (tile_width > 0 && "The tiles' width can't be 0");
    assert (tile_height > 0 && "The tile's height can't be 0");
    assert (tiles != 0 && "Invalid null texture to use as tiles in a map");

    for (MapIndex y = 0 ; y < rows ; ++y) {
        for (MapIndex x = 0 ; x < columns ; ++x) {
            this->map[y][x] = -1;
        }
    }

    int texture_width;
    int texture_height;
    SDL_QueryTexture (tiles.get (), 0, 0, &texture_width, &texture_height);
    this->tiles_cols = texture_width / tile_width;
    this->tiles_rows = texture_height / tile_height;

    assert (this->tiles_cols > 0 &&
            "Tile's width is too big for this texture");
    assert (this->tiles_rows > 0 &&
            "Tile's height is too big for this texture");
}

void TileMap::render (SDL_Renderer &renderer, int x, int y) const {
    SDL_Rect src = this->tile;

    SDL_Point pos;
    pos.y = y - Game::camera ().y * this->scroll_y;
    int first_tile_y =
        std::max (int ((Game::camera ().y - y) / this->tile.h), 0);
    int last_tile_y =
        std::min (first_tile_y + Game::screen ().height / this->tile.h + 2,
            int(this->rows ()));
    pos.x = x - Game::camera ().x * this->scroll_x;
    int first_tile_x =
        std::max (int((Game::camera ().x - x) / this->tile.w), 0);
    int last_tile_x =
        std::min (first_tile_x + Game::screen ().width / this->tile.w + 2,
                int(this->columns ()));

    SDL_Rect dest = this->tile;
    dest.y = pos.y + first_tile_y * this->tile.h;
    for (int y = first_tile_y ; y < last_tile_y ; ++y) {
        dest.x = pos.x + first_tile_x * this->tile.w;
        for (int x = first_tile_x ; x < last_tile_x ; ++x) {
            int index = this->map[y][x];
            if (index > -1) {
                src.x = index % this->tiles_cols * this->tile.w;
                src.y = index / this->tiles_cols * this->tile.h;
                SDL_RenderCopy (&renderer, this->tiles.get (), &src, &dest);
            }
            dest.x += dest.w;
        }
        dest.y += dest.h;
    }
}

void TileMap::setIndex (unsigned int x, unsigned int y, int index) {
    assert ( x < this->columns ());
    assert ( y < this->rows ());
    assert ( index < (this->tiles_cols * this->tiles_rows));

    this->map[y][x] = index;
}
