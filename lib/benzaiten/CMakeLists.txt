# FindAlsa got added in CMake 2.8
cmake_minimum_required(VERSION 3.0)

# The project's name.  This will be the name of the solution file
# for Microsoft Visual Studio, for example.
project(Benzaiten)

# Remove some warnings in MSVC.
if(MSVC)
    add_definitions(-D_SCL_SECURE_NO_WARNINGS)
endif(MSVC)

# Process subdirectories.
add_subdirectory(3rdparty)
add_subdirectory(doc)
add_subdirectory(examples)
add_subdirectory(lib)
add_subdirectory(benzaiten)
