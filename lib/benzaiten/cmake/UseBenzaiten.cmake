# Boost is required in order to build.
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIRS})

# Add benzaiten, SDL, and SDL_mixer directories to the list of includes.
include_directories(${Benzaiten_SOURCE_DIR})
include_directories(${Benzaiten_BINARY_DIR}/lib/SDL2)
include_directories(${Benzaiten_SOURCE_DIR}/lib/SDL2/include)
include_directories(${Benzaiten_SOURCE_DIR}/lib/SDL2_mixer)

# Remove some warnings in MSVC.
if(MSVC)
    add_definitions(-D_SCL_SECURE_NO_WARNINGS)
endif(MSVC)

##
# Adds a Windows resource.
#
macro(ADD_WIN32_RESOURCE rcfile source_list)
    if(WIN32)
        # Generate the resource file (rc) from the CMake template (rc.cmake)
        set(_win32_rc_file ${CMAKE_CURRENT_BINARY_DIR}/${rcfile})
        configure_file(${rcfile}.cmake ${_win32_rc_file} @ONLY)
        # Under MinGW we need to compile it manually.
        if(MINGW)
            set(_win32_res_file ${CMAKE_CURRENT_BINARY_DIR}/${rcfile}.obj)
            add_custom_command(OUTPUT ${_win32_res_file}
                COMMAND ${CMAKE_RC_COMPILER} -I${CMAKE_CURRENT_SOURCE_DIR}
                -o${_win32_res_file}
                ${_win32_rc_file})
            set_source_files_properties(${_win32_res_file} PROPERTIES GENERATED ON)
        endif(MINGW)
        list(APPEND ${source_list} ${_win32_res_file} ${_win32_rc_file})
    endif(WIN32)
endmacro(ADD_WIN32_RESOURCE)

##
# Tells to use a config.h file from a config.h.cmake template in the same
# directory.
macro(USE_CONFIG_H)
    # Create the config.h file, add this directory to the include directories
    # in order to find it, and tell the compiler to use it.
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake
        ${CMAKE_CURRENT_BINARY_DIR}/config.h @ONLY)
    include_directories(${CMAKE_CURRENT_BINARY_DIR})
    add_definitions(-DHAVE_CONFIG_H)
endmacro(USE_CONFIG_H)

##
# Adds a Benzaiten game's target.
#
macro(BENZAITEN_GAME exe_name headers sources)
    # Tell that the header files are actually headers.
    set_source_files_properties("${headers}" PROPERTIES HEADER_FILE_ONLY ON)

    # Add the executable creating a bundle in Mac OS X and a GUI application
    # in Windows.
    if (ANDROID)
        # Use always the same library name for games in benzaiten.
        set(name "game")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fexceptions")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fexceptions")
        add_library(${name} SHARED ${headers} ${sources}
            "${Benzaiten_SOURCE_DIR}/lib/SDL2/src/main/android/SDL_android_main.cpp"
        )
        target_link_libraries(${name} SDL2_jni)
    else (ANDROID)
        if (WIN32)
            set(_extra_sources ${Benzaiten_SOURCE_DIR}/lib/SDL2/src/main/windows/SDL_windows_main.c)
        endif (WIN32)
        set(name ${exe_name})
        add_executable(${name} WIN32 MACOSX_BUNDLE ${headers} ${sources} ${_extra_sources})
    endif (ANDROID)

    # Tell which libraries need to link against.
    target_link_libraries(${name} benzaiten)

    # Make the executable be .gpe in GP2X
    if(GP2X)
        set_target_properties(${name} PROPERTIES SUFFIX ".gpe")
    endif(GP2X)

    # Make the executable be .dge in Dingux.
    if(A320)
        set_target_properties(${name} PROPERTIES SUFFIX ".dge")
    endif(A320)

    # With MinGW we need to strip the debug information away from the executable.
    if(MINGW AND CMAKE_BUILD_TYPE MATCHES "Release" OR CMAKE_BUILD_TYPE MATCHES "MinSizeRel")
        # Use the strip program from MinGW
        find_program(STRIP_BIN strip)
        if(STRIP_BIN)
            # Get where the executable will be located.
            get_target_property(EXEC_LOCATION ${name} LOCATION)
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${STRIP_BIN} ARGS -S --strip-unneeded ${EXEC_LOCATION}
                COMMENT "Removing debug information from CXX executable ${name}${CMAKE_EXECUTABLE_SUFFIX}"
                )
        endif(STRIP_BIN)
    endif(MINGW AND CMAKE_BUILD_TYPE MATCHES "Release" OR CMAKE_BUILD_TYPE MATCHES "MinSizeRel")

    if(APPLE AND NOT IOS)
        set(BUNDLE_CONTENTS_DIR ${CMAKE_CURRENT_BINARY_DIR}/${name}.app/Contents)
        # Mac OS X needs the frameworks inside the bundle's framework directory.
        list(GET SDL_LIBRARY 0 SDL_FRAMEWORK)
        list(GET SDLMIXER_LIBRARY 0 SDLMIXER_FRAMEWORK)

        # We also need to create the `Resources' directory in the application's
        # bundle.  In there we'll create the individual resource directories
        # and then copy the actual resource files from the source directory.
        set(RESOURCES_DIR ${BUNDLE_CONTENTS_DIR}/Resources)

        # Graphic resources.
        set(GFX_DIR ${RESOURCES_DIR}/gfx)
        foreach(GFX_FILE ${GFX_FILES}) # from gfx's CMakeLists.txt file
            get_filename_component(FILENAME ${GFX_FILE} NAME)
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${GFX_DIR}
                COMMAND ${CMAKE_COMMAND} ARGS -E copy ${GFX_FILE} ${GFX_DIR}
                COMMENT "Copying graphic file ${FILENAME} into resources directory")
        endforeach(GFX_FILE)
        if(MACOSX_BUNDLE_ICON_FILE)
            # as well as the icon.
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_COMMAND} ARGS -E copy ${CMAKE_SOURCE_DIR}/assets/gfx/${MACOSX_BUNDLE_ICON_FILE} ${RESOURCES_DIR}
                COMMENT "Copying icons file ${MACOSX_BUNDLE_ICON_FILE} into resources directory")
        endif(MACOSX_BUNDLE_ICON_FILE)

        # Music resources.
        set(MUSIC_DIR ${RESOURCES_DIR}/music)
        foreach(MUSIC_FILE ${MUSIC_FILES}) # from music's CMakeLists.txt file.
            get_filename_component(FILENAME ${MUSIC_FILE} NAME)
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${MUSIC_DIR}
                COMMAND ${CMAKE_COMMAND} ARGS -E copy ${MUSIC_FILE} ${MUSIC_DIR}
                COMMENT "Copying music file ${FILENAME} into resources directory")
        endforeach(MUSIC_FILE)

        # Sound resources.
        set(SFX_DIR ${RESOURCES_DIR}/sfx)
        foreach(SFX_FILE ${SFX_FILES}) # from sfx's CMakeLists.txt file.
            get_filename_component(FILENAME ${SFX_FILE} NAME)
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${SFX_DIR}
                COMMAND ${CMAKE_COMMAND} ARGS -E copy ${SFX_FILE} ${SFX_DIR}
                COMMENT "Copying sound file ${FILENAME} into resources directory")
        endforeach(SFX_FILE)

        # Stages resources.
        set(STAGES_DIR ${RESOURCES_DIR}/stages)
        foreach(STAGE_FILE ${STAGE_FILES}) # from stages' CMakeLists.txt file.
            get_filename_component(FILENAME ${STAGE_FILE} NAME)
            add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${STAGES_DIR}
                COMMAND ${CMAKE_COMMAND} ARGS -E copy ${STAGE_FILE} ${STAGES_DIR}
                COMMENT "Copying stage file ${FILENAME} into resources directory")
        endforeach(STAGE_FILE)

        # Add a "bundle" target that creates the correct DMG file.
        set(BUNDLE_NAME ${name}-${MACOSX_BUNDLE_SHORT_VERSION_STRING}.dmg)
        add_custom_target(bundle
            rm -f ${BUNDLE_NAME}
            COMMAND hdiutil create -srcfolder ${CMAKE_CURRENT_BINARY_DIR}/${name}.app -volname "${MACOSX_BUNDLE_BUNDLE_NAME}" ${BUNDLE_NAME}
            COMMAND hdiutil internet-enable -yes ${BUNDLE_NAME}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
            COMMENT "Generating DMG bundle ${BUNDLE_NAME}")
    endif(APPLE AND NOT IOS)

    if (ANDROID)
        add_custom_command(TARGET ${name} POST_BUILD
            COMMAND ${CMAKE_COMMAND} ARGS -E copy "${STL_LIBRARIES_PATH}/libgnustl_shared.so" ${LIBRARY_OUTPUT_PATH}
            COMMENT "Copying gnustl_shared library")
    endif (ANDROID)

    # Install the binary files to the bin directory.
    install(TARGETS ${name} RUNTIME DESTINATION bin
        LIBRARY DESTINATION libs
        BUNDLE DESTINATION Applications)
endmacro(BENZAITEN_GAME)
