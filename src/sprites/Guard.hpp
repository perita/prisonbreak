//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_PRISON_BREAK_SPRITES_GUARD_HPP)
#define GEISHA_STUDIOS_PRISON_BREAK_SPRITES_GUARD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Point2D.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace prisonbreak {
    ///
    /// @class Guard
    /// @brief A guard is a spotlight.
    ///
    class Guard: public benzaiten::Sprite {
        public:
            /// A pointer to a Guard instance.
            typedef boost::shared_ptr<Guard> Ptr;
            /// The Speed type.
            typedef benzaiten::Point2D<double> Speed;

            /// The guard's direction.
            enum Direction {
                LEFT = -1,
                RIGHT = 1
            };

            ///
            /// @brief Creates a new Guard.
            ///
            /// @param[in] x The initial X position.
            /// @param[in] direction The guard's initial direction.
            /// @param[in] speed The guard's speed.
            ///
            static Ptr New (double x, Direction direction,
                    const Speed &speed) {
                return Ptr (new Guard (x, direction, speed));
            }

            virtual void update (double elapsed);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] x The initial X position.
            /// @param[in] direction The guard's initial direction.
            /// @param[in] speed The guard's speed.
            ///
            Guard(double x, Direction direction, const Speed &speed);

            /// The guard's direction.
            int direction;
            /// The guard's speed.
            Speed speed;
    };
}

#endif // !GEISHA_STUDIOS_PRISON_BREAK_SPRITES_GUARD_HPP
