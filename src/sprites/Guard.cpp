//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Guard.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>

using namespace prisonbreak;

Guard::Guard(double x, Direction direction, const Speed &speed)
    : Sprite (x, -15.0)
    , direction (direction)
    , speed (speed)
{
    using benzaiten::Texture;

    this->type = Types::GUARD;

    Texture::Ptr spotlight = Texture::New ("spotlight.tga");
    spotlight->centerOrigin ();
    this->setGraphic (spotlight);
}

void Guard::update (double elapsed) {
    using benzaiten::Game;

    this->advance (
            this->speed.x * this->direction * elapsed,
            this->speed.y * elapsed);

    if (this->x () < 0) {
        this->x (0);
        this->direction = 1;
    } else if (this->x () > Game::screen().width) {
        this->x (Game::screen().width);
        this->direction = -1;
    }

    if (this->y () > Game::screen().height + 15) {
        this->context->remove (this);
    }
}

/*
void
Guard::update(int left, int right, double vspeed)
{
    x_ += speed_ * direction_;
    if (Left == direction_ && x_ < left)
    {
        x_ += (left - x_);
        direction_ = Right;
    }
    else if (Right == direction_ && x_ > right)
    {
        x_ -= (x_ - right);
        direction_ = Left;
    }

    y_ += vspeed;
}
*/
