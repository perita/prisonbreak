//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP)
#define GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <SDL_stdinc.h>
#include <SDL_pixels.h>
#include <boost/shared_ptr.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include "../Graphic.hpp"
#include "../Point2D.hpp"

namespace benzaiten {
    ///
    /// @class StarField
    /// @brief Shows an star filed with multiple star layers.
    ///
    class StarField: public Graphic {
        public:
            /// Pointer to a StarField instance.
            typedef boost::shared_ptr<StarField> Ptr;

            ///
            /// @brief Creates a new instance of a StarField.
            ///
            /// @param[in] width The width to limit the stars' position to.
            /// @param[in] height The height to limit the stars' position to.
            ///
            static Ptr New (int width, int height) {
                return Ptr (new StarField (width, height));
            }

            ///
            /// @brief Adds a new layer of stars.
            ///
            /// @param[in] stars The number of stars in the layer.
            /// @param[in] color The stars' color.
            /// @param[in] speed The stars' speed.
            ///
            void add (unsigned int stars, const SDL_Color &color,
                    int speed);

            virtual void render (SDL_Renderer &renderer, int x, int y) const;
            virtual void update (double elapsed);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] width The width to limit the stars' position to.
            /// @param[in] height The height to limit the stars' position to.
            ///
            StarField(int width, int height);

        private:
            typedef boost::mt19937 RandomNumberGenerator;
            typedef boost::variate_generator<RandomNumberGenerator&, boost::uniform_int<> > RandomCoordinateGenerator;

            ///
            /// @struct Layer
            /// @brief A star layer.
            ///
            struct Layer {
                /// The layer's star color.
                SDL_Color color;
                /// The layer's speed.
                int16_t speed;
                /// The stars in this layer.
                std::vector<Point2D<double> > stars;

                ///
                /// @brief Constructor.
                ///
                /// @param[in] color The layer's color.
                /// @param[in] speed The layer's speed.
                /// @param[in] num_stars The number of stars of this layer.
                /// @param[inout] randomX The random coordinate generator
                ///               for X.
                /// @param[inout] randomY The random coordinate generator
                ///               for Y.
                ///
                Layer(const SDL_Color &color, int speed,
                        unsigned int num_stars,
                        RandomCoordinateGenerator &randomX,
                        RandomCoordinateGenerator &randomY);
            };

            /// The height of the field.
            size_t height;
            /// All the stars in layers.
            std::vector<Layer> layers;
            /// The randon numeber generator.
            RandomNumberGenerator random_generator;
            /// The width of the field.
            size_t width;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP
