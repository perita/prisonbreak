//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GAME_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GAME_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <cassert>
#include <string>
#include <SDL_main.h>
#include <boost/noncopyable.hpp>
#include "Audio.hpp"
#include "Context.hpp"
#include "InputManager.hpp"
#include "ResourceManager.hpp"
#include "Screen.hpp"
#include "FrameRateManager.hpp"

namespace benzaiten {
    ///
    /// @class Game
    /// @brief The main game's system class.
    ///
    /// Each game is supposted to have a derived class from this and set up
    /// all the game specific options inside the derived class' constructor.  In
    /// main, the only thing that should be done is to create an instance of
    /// this derived class and call the operator()() function.
    ///
    class Game: public boost::noncopyable {
        public:
            ///
            /// @brief Constructor.
            ///
            /// Initializes the game's environment to run properly, creates the
            /// game won't start until the operator()() member function is called.
            ///
            /// @param[in] width The width of the game's window.
            /// @param[in] height The height of the game's window.
            /// @param[in] title The title to set to the window.
            /// @param[in] game_id The identifier for the game.  This is
            ///            the same of the directory where the resources are
            ///            located at.
            /// @param[in] enable_audio Whether to enable the audio subsystem.
            /// @param[in] fps The frame per second that the game should run.
            ///
            Game (int width, int height, const std::string &title,
                    const std::string &game_id, bool enable_audio = true, double fps = 35.0);

            ///
            /// @brief Destructor.
            ///
            ~Game ();

            ///
            /// @brief Returns the reference to the game's camera.
            ///
            /// @return The reference to the game's camera.
            ///
            static Point2D<double> &camera () {
                assert (the_game != 0);
                return the_game->game_camera;
            }

            ///
            /// @brief Returns the reference to the game's input manager.
            ///
            /// @return The reference to the game's input manager.
            ///
            static InputManager &input () {
                assert (the_game != 0);
                return *the_game->input_manager;
            }

            ///
            /// @brief Starts the execution of the game's loop.
            ///
            /// The game loop updates and the rendres the active Context once
            /// for loop and makes sure that the game doesn't run faster that
            /// the expected frames per second.
            ///
            /// The game loop ends when there is no active Context object.
            ///
            void operator() ();

            ///
            /// @brief Quits from the game.
            ///
            static void quit ();

            ///
            /// @brief Returns a reference to the resource manager.
            ///
            /// @return The reference to the resource manager.
            ///
            static ResourceManager &resources () {
                assert (the_game != 0);
                return *(the_game->resource_manager);
            }

            ///
            /// @brief Returns the reference to the game's screen.
            ///
            /// @return The reference to the game's screen.
            ///
            static Screen &screen () {
                assert (the_game != 0);
                return *(the_game->game_screen);
            }

            ///
            /// @brief Sets the screen's fill policy.
            ///
            /// @param policy The policy to set.
            ///
            void setScreenFillPolicy (Screen::FillPolicy policy) {
                this->screen_fill_policy = policy;
            }

            ///
            /// @brief Switches to a new context.
            ///
            /// This tells the game to switch to a new context at the end of
            /// the current frame; it won't switch until the current context
            /// is completely updated and rendered.
            ///
            /// The Game object is responsible to delete the context when
            /// no longer needed.
            ///
            /// @param[in] context The new context to switch to.  If the new
            ///            @p context is @c NULL, then the game ends.
            ///
            static void switchTo (Context::Ptr context);

        protected:
            ///
            /// @brief Called when the Game engine has been initialized
            ///
            /// Until this function is called, it is not safe to call
            /// any of the static member functions of game as the
            /// engine is not yet ready.  This means that resources loading
            /// and such must happen either in this function or later.
            ///
            virtual void init () = 0;

            ///
            /// @brief Called when the Game engine is going to shutdown.
            ///
            /// After this function is called, you should not longer access
            /// static member functions of Game (such as resources).
            ///
            virtual void cleanUp () { }

        private:
            ///
            /// @brief Initializes the SDL library.
            ///
            struct SdlInit {
                ///
                /// @brief Initializes SDL.
                ///
                /// @brief audio Whether to enable the audio subsystem.
                ///
                SdlInit (bool audio);

                ///
                /// @brief Tears down SDL.
                ///
                ~SdlInit ();
            };

            ///
            /// @brief Processes the event queue.
            ///
            /// @param input_manager The input manager to forward events to.
            ///
            void processEvents (InputManager &input_manager);

	    ///
	    /// @brief Processes one step of the main loop.
	    ///
	    void runOnce();

	    ///
	    /// @brief Processes one step of the main loop of a game.
	    ///
	    /// @param game The game to process the step of.
	    ///
	    static void runOnce(void *game);

            /// This is the unique instance of the game object.
            static Game *the_game;

            /// The currently active context.
            Context::Ptr active_context;
            /// Whether to enable audio.
            bool enable_audio;
            /// The game's camera.
            Point2D<double> game_camera;
            /// The game's id.
            const std::string game_id;
            /// The game's screen.
            Screen *game_screen;
            /// The input manager.,
            InputManager *input_manager;
            /// The previous active context.
            Context::Ptr previous_context;
            /// The resource manager.
            ResourceManager *resource_manager;
            /// The screen's fill policy.
            Screen::FillPolicy screen_fill_policy;
            /// The screen's height.
            int screen_height;
            /// The screen's title.
            const std::string screen_title;
            /// The screen's width.
            int screen_width;
            /// Frame rate manager.
            FrameRateManager frameRate;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GAME_HPP
