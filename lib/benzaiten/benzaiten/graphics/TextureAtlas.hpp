//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TEXTURE_ATLAS_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TEXTURE_ATLAS_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <map>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include "Animation.hpp"
#include "Texture.hpp"

namespace benzaiten {
    ///
    /// @class TextureAtlas
    /// @brief Cuts a Surface in different subviews.
    ///
    class TextureAtlas: public Texture {
        public:
            /// A pointer to a TextureAtlas instance.
            typedef boost::shared_ptr<TextureAtlas> Ptr;

            ///
            /// @brief Initializes a TextureAtlas with a Texture.
            ///
            /// @param[in] data The pointer to the texture to use.
            /// @param[in] width The width of each of the elements in
            ///            the sheet.  Must be greater than 0.
            /// @param[in] height The height of each of the elements in
            ///            the sheet. If <= 0 then the same value as \p width
            ///            is used.
            ///
            static Ptr New (const SDL::Texture &data, int width,
                    int height = -1) {
                return Ptr (new TextureAtlas (data, width, height));
            }

            ///
            /// @brief Creates a TextureAtlas with a Texture but no frames.
            ///
            /// @param[in] data The pointer to the texture to use.
            ///
            static Ptr New (const SDL::Texture &data) {
                return Ptr (new TextureAtlas (data));
            }

            ///
            /// @brief Adds a new single-frame animation.
            ///
            /// @param[in] id The identifier for the new animation.
            /// @param[in] frmae The index of the sole frame for this animation.
            ///
            void add (const std::string &id, unsigned int frame);

            ///
            /// @brief Adds a new animation.
            ///
            /// @param[in] id The identifier for the new animations.
            /// @param[in] frames The index of the frames that compose the
            ///            animation.
            /// @param[in] rate The animation's frame rate.
            /// @param[in] loop Whether the animation starts again or stays
            ///            at the last frame when done.
            ///
            void add (const std::string &id,
                    const std::vector<unsigned int> &frames, double rate,
                    bool loop);

            ///
            /// @brief Gets the height of all sprites in the sheet.
            ///
            /// @return The height of each individual sprite.
            ///
            int height () const;

            ///
            /// @brief Creates a new frame definition.
            ///
            /// @param[in] x The starting X position of the frame.
            /// @param[in] y The starting Y position of the frame.
            /// @param[in] width The frame's width.
            /// @param[in] height The frame's height.
            ///
            /// @return The index of the new frame.
            ///
            unsigned int newFrame (int x, int y, int width, int heigth);

            ///
            /// @brief Gets the number of frames in the atlas.
            ///
            /// @return The number of frames in the atlas.
            ///
            unsigned int numFrames () const;

            ///
            /// @brief Starts playing an animation.
            ///
            /// @param[in] id The identifier for the animation to play.
            ///
            /// @return The Animation that is being played.
            ///
            void play (const std::string &id);

            virtual void update (double elapsed);

            ///
            /// @breief Gets the width of all sprites in the sheet.
            ///
            /// @return The width of each individual sprite.
            ///
            int width () const;

        protected:
            ///
            /// @brief Initializes a TextureAtlas with a Texture.
            ///
            /// @param[in] data The pointer to the texture to use.
            /// @param[in] width The width of each of the elements in
            ///            the sheet.  Must be greater than 0.
            /// @param[in] height The height of each of the elements in
            ///            the sheet. If <= 0 then the same value as \p width
            ///            is used.
            ///
            TextureAtlas (const SDL::Texture &data, int width,
                    int height = -1);

            ///
            /// @brief Creates a TextureAtlas with a Texture but no frames.
            ///
            /// @param[in] data The pointer to the texture to use.
            ///
            TextureAtlas (const SDL::Texture &data);

        private:
            /// The type of a animation map.
            typedef std::map<std::string, Animation> AnimationMap;

            ///
            /// @brief Sets the current atlas' frame.
            ///
            /// @param[in] index The index of the frame to set.  It must be
            ///            less that countFrames().
            ///
            void setFrame (unsigned int frame);

            ///
            /// @brief Sets up the Texture frames.
            ///
            /// @param[in] width The width of each frame.
            /// @param[in] height The height of each frame.  If set to -1
            ///            it is assumed to be the same as @p width.
            ///
            void setUpFrames (int width, int height);

            /// The current animation.
            Animation animation;
            /// All the available animations.
            AnimationMap animations;
            /// The current frame index.
            unsigned int frame_index;
            /// The individial frames.
            std::vector<SDL_Rect> frames;
            /// The current animation time.
            double time;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_TEXTURE_ATLAS_HPP
