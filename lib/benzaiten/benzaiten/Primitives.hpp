//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_PRIMITIVES_HPP)
#define GEISHA_STUDIOS_BENZAITEN_PRIMITIVES_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

// Forward declarations.
class SDL_Renderer;
struct SDL_Color;

namespace benzaiten {
    ///
    /// @brief Renders a filled circle.
    ///
    /// @param[in] renderer The renderer to render the circle to.
    /// @param[in] cx The X position of the circle's center.
    /// @param[in] cy The Y position of the circle's center.
    /// @param[in] radius The circle's radius.
    /// @param[in] color The color to fill the circle with.
    ///
    void renderFillCircle (SDL_Renderer &renderer, int cx, int cy, int radius, const SDL_Color &color);
}

#endif // !GEISHA_STUDIOS_BENZAITEN_PRIMITIVES_HPP
