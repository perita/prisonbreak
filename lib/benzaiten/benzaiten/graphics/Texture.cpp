//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Texture.hpp"
#include "../Game.hpp"
#include "../Utils.hpp"

using namespace benzaiten;

Texture::Texture ():
    clip (makeRect (0, 0, 0, 0)),
    data (),
    origin (0, 0),
    size (makeRect (0, 0, 0, 0))
{
}

Texture::Texture (const std::string &file_name):
    clip (makeRect (0, 0, 0, 0)),
    data (),
    size (makeRect (0, 0, 0, 0))
{
    this->setTexture (Game::resources ().texture (file_name));
}

Texture::Texture (const SDL::Texture &data):
    clip (makeRect (0, 0, 0, 0)),
    data (),
    size (makeRect (0, 0, 0, 0))
{
    this->setTexture (data);
}

void Texture::centerOrigin () {
    this->origin.x = this->clip.w / 2;
    this->origin.y = this->clip.h / 2;
}

void Texture::render (SDL_Renderer &render, int x, int y) const {
    if (this->data != 0) {
        SDL_Rect dest =
            makeRect (
                    x - this->origin.x - Game::camera ().x * this->scroll_x,
                    y - this->origin.y - Game::camera ().y * this->scroll_y,
                    this->clip.w, this->clip.h);
        SDL_RenderCopy (&render, this->data.get (), &(this->clip), &dest);
    }
}

void Texture::setClip (int x, int y, int width, int height) {
    this->clip.x = x;
    this->clip.y = y;
    this->clip.w = width;
    this->clip.h = height;
}

void Texture::setTexture (const SDL::Texture &texture) {
    this->data = texture;
    SDL_QueryTexture (this->data.get (), 0, 0, &(this->size.w), &(this->size.h));
    this->setClip (0, 0, this->size.w, this->size.h);
}
