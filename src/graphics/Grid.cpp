//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Grid.hpp"
#include <benzaiten/Game.hpp>

using namespace prisonbreak;

namespace {
    const double CELL_HEIGHT = 24.0;
    const double CELL_WIDTH = 32.0;
}

Grid::Grid (double speed)
    : Graphic ()
    , offset (0.0)
    , speed (speed)
{
    this->setActive (true);
}

void Grid::render (SDL_Renderer &renderer, int, int) const {
    const benzaiten::Screen &screen (benzaiten::Game::screen ());

    SDL_SetRenderDrawColor (&renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    for (int x = CELL_HEIGHT / 2; x < screen.width ; x += CELL_HEIGHT) {
        SDL_RenderDrawLine (&renderer, x, 0, x, screen.height);
    }
    for (int y = CELL_WIDTH / -2 + offset; y < screen.height ; y += CELL_HEIGHT) {
        SDL_RenderDrawLine (&renderer, 0, y, screen.width, y);
    }
}

void Grid::update (double elapsed) {
    this->offset += this->speed * elapsed;
    while (this->offset > CELL_HEIGHT) {
        this->offset -= CELL_HEIGHT;
    }
}
