//
// Balls -- Benzaiten's sample game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <benzaiten/ErrorMessage.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include "contexts/Start.hpp"

///
/// @class Main
/// @brief The game's entry point.
///
/// This class' responsibilities are to initialize the game engine
/// (benzaiten::Game) by telling the screen size, game id, and
/// window's title.
///
/// The game id is used later to find resources, depending on the
/// platform.  Under Unix, it is expected that the resources are
/// located inside "../share/${game_id}/" from the game's executable
/// folder.
///
class Main: public benzaiten::Game {
    public:
        Main ()
                          // width, height, title, game id, no audio
            : benzaiten::Game (320, 480, "Balls!", "balls", false)
        {
        }

    protected:
        virtual void init () {
            // Set the screen's color used to clear the screen (i.e.,
            // the backgroubnd color.
            Game::screen().color = benzaiten::makeColor (42, 84, 126);

            // Switch to the Start context.
            this->switchTo (Start::New (1, true));
        }
};

int main (int argc, char *argv[]) {
    // Set up random number generator.
    std::srand (std::time (0));

    // Start the game engine.
    Main game;

    try {
        // The game loop; this returns when there is nothing else
        // to do in the game.
        game();

        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        // Show the message in the most appropriate way according
        // to the platform.
        benzaiten::showErrorMessage (e.what ());
    } catch (...) {
        // A generic "unknown error" message.
        benzaiten::showUnknownErrorMessage ();
    }
    return EXIT_FAILURE;
}
