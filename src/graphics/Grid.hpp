//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_PRISON_BREAK_GRAPHICS_GRID_HPP)
#define GEISHA_STUDIOS_PRISON_BREAK_GRAPHICS_GRID_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Graphic.hpp>
#include <boost/shared_ptr.hpp>

namespace prisonbreak {
    class Grid: public benzaiten::Graphic {
        public:
            /// A pointer to a grid.
            typedef boost::shared_ptr<Grid> Ptr;

            static Ptr New (double speed) { return Ptr (new Grid (speed)); }

            virtual void render (SDL_Renderer &renderer, int x, int y) const;
            virtual void update (double elapsed);

        protected:
            Grid (double speed);

            double offset;
            double speed;
    };
}

#endif // !GEISHA_STUDIOS_PRISON_BREAK_GRAPHICS_GRID_HPP
