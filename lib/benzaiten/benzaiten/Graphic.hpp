//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GRAPHIC_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GRAPHIC_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>

// Forward declarations.
class SDL_Renderer;

namespace benzaiten {
    ///
    /// @class Graphic
    /// @brief Base class for all graphic implementations for Sprite.
    ///
    /// The Graphic class is an interface to ease the use of graphics within
    /// the Sprite class.
    ///
    class Graphic {
        public:
            /// A pointer to a Graphic.
            typedef boost::shared_ptr<Graphic> Ptr;

            ///
            /// @brief Constructor.
            ///
            Graphic ()
                : scroll_x (1.0)
                , scroll_y (1.0)
                , active (false)
                , visible (true)
            {
            }

            ///
            /// @brief Destructor.
            ///
            virtual ~Graphic () { }

            ///
            /// @brief Tells whether the graphic is active.
            ///
            /// @return @c true if the graphic is active.
            ///
            bool isActive () const { return this->active; }

            ///
            /// @brief Tells whether the graphic is visible.
            ///
            /// @return @c true if the graphic is visible.
            ///
            bool isVisible () const { return this->visible; }

            ///
            /// @brief Renders the graphic.
            ///
            /// @param[inout] renderer The renderer to use to render the
            ///               graphic.
            /// @param[in] x The X position where to render the Graphic.
            /// @param[in] y The Y position where to render the Graphic.
            ///
            virtual void render (SDL_Renderer &renderer, int x, int y) const = 0;

            ///
            /// @brief Sets whether the Graphic is active.
            ///
            /// @param[in] active Set to @c true to set the graphic active.
            ///
            void setActive (bool active) { this->active = active; }

            ///
            /// @brief Sets how much the camera affect's the graphic's position.
            ///
            /// @param x The amount that the camera affects the X position. e.g.,
            ///          0.0 means follow the camera, 0.5 half the camera, etc.
            /// @param y The amoun that the camera affecta the Y position, similar
            ///          to @p x.
            ///
            void setScroll (double x, double y) {
                this->scroll_x = x;
                this->scroll_y = y;
            }

            ///
            /// @brief Sets whether the Graphic is visible.
            ///
            /// @param[in] visible Set to @c true to show the graphic.
            ///
            void setVisible (bool visible) { this->visible = visible; }

            ///
            /// @brief Updates the graphic's state.
            ///
            /// @param[in] elapsed The seconds elapsed since the last call to this
            ///            function.
            ///
            virtual void update (double elapsed) { }

        protected:
            /// The amount the camera affects the graphic's X position.
            double scroll_x;
            /// The amount the camera affects the graphic's Y position.
            double scroll_y;

        private:
            /// Whether the graphic is active.
            bool active;
            /// Whether the graphic is visible.
            bool visible;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GRAPHIC_HPP
