//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_EXPLOSION_HPP)
#define GEISHA_STUDIOS_EXPLOSION_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL_stdinc.h>
#include <SDL_pixels.h>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

///
/// @class Explosion
/// @brief A filled circle that grows to a maximum radius.
///
/// The Explosion is a filled circle that starts at a given radius and
/// then grows until it reached a certain radius.  At that point, it will
/// shrink until it has a radius of 0 and will remove itself from its
/// context.
///
class Explosion: public benzaiten::Sprite {
    public:
        /// A smart pointer to a Explosion sprite.
        typedef boost::shared_ptr<Explosion> Ptr;

        ///
        /// @brief Create a smart pointer to an Explosion.
        ///
        /// @param[in] x The X position to create the Explosion to.
        /// @param[in] y The Y postiion to create the Explosion to.
        /// @param[in] color The Explosion's color.
        /// @param[in] radius The Explosion's initial radius.
        ///
        /// @return The smart pointer to the newly created Explosion.
        ///
        static Ptr New (int x, int y, const SDL_Color &color,
                double radius = 1.0) {
            return Ptr (new Explosion (x, y, color, radius));
        }

        ///
        /// @brief Gets the Explosion's current radius.
        ///
        /// @return The radius in pixels that the Explosion currently has.
        ///
        double getRadius () const { return radius; }

        virtual void render (SDL_Renderer &renderer) const;
        virtual void update (double elapsed);

    protected:
        ///
        /// @brief Constructor.
        ///
        /// @param[in] x The X position to create the Explosion to.
        /// @param[in] y The Y postiion to create the Explosion to.
        /// @param[in] color The Explosion's color.
        /// @param[in] radius The Explosion's initial radius.
        ///
        /// @return The smart pointer to the newly created Explosion.
        ///
        Explosion (int x, int y, const SDL_Color &color,
                double radius = 1.0);

    private:
        /// The explosion's color.
        SDL_Color color;
        /// The Explosion's growth direction.
        int dir;
        /// The current Explosion's radius.
        double radius;
};

#endif // !GEISHA_STUDIOS_EXPLOSION_HPP
