#!/bin/bash
set -e
rm -fr build/unix
mkdir -p build/unix
cd build/unix
cmake -DCMAKE_CXX_FLAGS="-Wall" -DCMAKE_C_FLAGS="-Wall" -DCMAKE_OSX_ARCHITECTURES="i386;ppc" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_INSTALL_PREFIX="/tmp" ../..
make
if [ `uname -s` = "Darwin" ]
then
    make bundle
else
    make install
fi
