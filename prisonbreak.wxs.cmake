<?xml version='1.0' encoding='latin1'?>
<Wix xmlns='http://schemas.microsoft.com/wix/2006/wi'
     xmlns:util='http://schemas.microsoft.com/wix/UtilExtension'>
  <Product Id='{7695A5F6-DA51-4FD7-BFCC-ED7BBB4B95D8}' Name='Prison Break' Language='1033' Version='@VERSION@' Manufacturer='Geisha Studios' UpgradeCode='{2767EE2A-CB74-4F1F-930C-A643C7FF1F08}'>
    <Package Description='Espace from prison' Manufacturer='Geisha Studios' Keywords='casual prison escape game' InstallerVersion='200' Compressed='yes' Platform='x86'/>
    <Media Id='1' Cabinet='prisonbreak.cab' EmbedCab='yes'/>
    <Property Id='WIXUI_INSTALLDIR' Value='INSTALLFOLDER'/>
    <Upgrade Id='{28EF970B-BA65-477E-8A19-7DE23E7263E6}'>
      <UpgradeVersion Minimum='@VERSION@' IncludeMinimum='no' OnlyDetect='yes' Language='1033' Property='NEWPRODUCTFOUND'/>
<!--      <UpgradeVersion Minimum='1.0.0.0' IncludeMinimum='yes' Maximum='@VERSION@' IncludeMaximum='no' Language='1033' Property='UPGRADEFOUND'/>-->
    </Upgrade>
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOX' Value='1' />
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOXTEXT' Value='Launch Prison Break' />
    <Property Id='WixShellExecTarget' Value='[#prisonbreak.exe]'/>
    <CustomAction Id='LaunchGame' BinaryKey='WixCA' DllEntry='WixShellExec' Impersonate='yes'/>
    <UI>
      <UIRef Id='WixUI_InstallDir'/>
      <UIRef Id='WixUI_ErrorProgressText'/>
      <Publish Dialog='ExitDialog' Control='Finish' Event='DoAction' Value='LaunchGame'>WIXUI_EXITDIALOGOPTIONALCHECKBOX = 1 and NOT Installed</Publish>
    </UI>
    <Icon Id="prisonbreak.ico" SourceFile='@CMAKE_SOURCE_DIR@/data/gfx/prisonbreak.ico'/>
    <Property Id="ARPPRODUCTICON" Value="prisonbreak.ico" />

    <Directory Id='TARGETDIR' Name='SourceDir' DiskId='1'>
      <Directory Id='ProgramFilesFolder' Name='PFiles'>
        <Directory Id='GeishaStudiosFolder' Name='Geisha Studios'>
          <Directory Id='INSTALLFOLDER' Name='Prison Break'>
            <Component Id='PrisonBreakBinaryComponent' Guid='{4815670B-0C8C-400C-A0B2-8AF3C15EFB9B}'>
             <File Source='@CMAKE_BINARY_DIR@/src/prisonbreak.exe' KeyPath='yes'/>
            </Component>
            <Component Id='SDLBinaryComponent' Guid='{1EF25407-F620-4482-8788-D3A87823BF60}'>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL/SDL.dll' KeyPath='yes'/>
            </Component>
            <Component Id='SDLmixerBinaryComponent' Guid='{B147AC34-C429-4C17-B924-C9879FC6205F}'>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/SDL_mixer.dll' KeyPath='yes'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/ogg.dll'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbis.dll'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbisfile.dll'/>
            </Component>
            <Directory Id='GraphicsFolder' Name='gfx'>
              <Component Id='PrisonBreakGraphicsComponent' Guid='{C4AB1981-B26B-4858-9A22-7B14276A7861}'>
                <?foreach GRAPHIC in @GFX_FILES@?>
                <File Source='$(var.GRAPHIC)'/>
                <?endforeach?>
              </Component>
            </Directory>
            <Directory Id='MusicFolder' Name='music'>
              <Component Id='PrisonBreakMusicComponent' Guid='{B794B737-0092-49A4-A150-942E676E260C}'>
                <?foreach MUSIC in @MUSIC_FILES@?>
                <File Source='$(var.MUSIC)'/>
                <?endforeach?>
              </Component>
            </Directory>
            <Directory Id='SoundsFolder' Name='sfx'>
              <Component Id='PrisonBreakSoundsComponent' Guid='{15C6D4AC-65A1-4CFF-A2A5-6E061B36378D}'>
                <?foreach SFX in @SFX_FILES@?>
                <File Source='$(var.SFX)'/>
                <?endforeach?>
              </Component>
            </Directory>
          </Directory>
        </Directory>
      </Directory>
      <Directory Id='ProgramMenuFolder' Name='PMenu'>
        <Directory Id='PrisonBreakProgramMenuFolder' Name='Prison Break'>
          <Component Id='PrisonBreakStartMenuShortcutComponent' Guid='{EBB6190D-99C2-4AD9-8F05-4676C8406DA4}'>
            <Shortcut Id='PrisonBreakStartMenuShortcut' Name='Prison Break' Description='Escape from prison' Target='[INSTALLFOLDER]prisonbreak.exe' WorkingDirectory='INSTALLFOLDER'/>
            <util:InternetShortcut Id='PrisonBreakHomeShortcut' Directory='PrisonBreakProgramMenuFolder' Name='Prison Break&apos; Home Page' Target='http://www.geishastudios.com/games/prisonbreak/'/>
            <RemoveFolder Id='PrisonBreakProgramMenuFolder' On='uninstall'/>
            <RegistryValue Root='HKCU' Key='Software\Geisha Studios\PrisonBreak' Name='StartMenuShortcut' Type='integer' Value='1' KeyPath='yes'/>
          </Component>
        </Directory>
      </Directory>
    </Directory>

    <CustomAction Id='PreventDowngrading' Error='There is a newer version already installed'/>

    <InstallExecuteSequence>
      <Custom Action='PreventDowngrading' After='FindRelatedProducts'>NEWPRODUCTFOUND</Custom>
      <RemoveExistingProducts After='InstallFinalize'/>
    </InstallExecuteSequence>

    <InstallUISequence>
      <Custom Action='PreventDowngrading' After='FindRelatedProducts'>NEWPRODUCTFOUND</Custom>
    </InstallUISequence>

    <Feature Id='PrisonBreak' Title='PrisonBreak' Level='1' Display='expand' AllowAdvertise='no'>
      <ComponentRef Id='PrisonBreakBinaryComponent'/>
      <ComponentRef Id='PrisonBreakGraphicsComponent'/>
      <ComponentRef Id='PrisonBreakStartMenuShortcutComponent'/>
      <ComponentRef Id='PrisonBreakMusicComponent'/>
      <ComponentRef Id='PrisonBreakSoundsComponent'/>
      <ComponentRef Id='SDLBinaryComponent'/>
      <ComponentRef Id='SDLmixerBinaryComponent'/>
    </Feature>
  </Product>
</Wix>
