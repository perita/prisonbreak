//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Screen.hpp"
#include <stdexcept>
#include <SDL_error.h>
#include <SDL_render.h>
#include "Utils.hpp"

#if defined (ANDROID)
#include <SDL_opengles.h>
#else // !ANDROID
#include <SDL_opengl.h>
#endif // ANDROID

using namespace benzaiten;

Screen::Screen (int width, int height, const std::string &title,
        FillPolicy fill_policy)
    : color (makeColor (0, 0, 0))
    , half_height ()
    , half_width ()
    , height ()
    , renderer ()
    , translation ()
    , scale (1)
    , width ()
    , window ()
{
    Uint32 window_flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;

#if defined (GP2X) || defined (A320) || \
    defined (PANDORA) || defined (ANDROID) || defined (IOS)
    window_flags |= SDL_WINDOW_FULLSCREEN | SDL_WINDOW_BORDERLESS;
#endif // GP2X || A320 || PANDORA || ANDROID || IOS

#if defined (GP2X) || defined (A320)
    window_flags &= ~SDL_WINDOW_OPENGL;
    width = 320;
    height = 240;
#endif // GP2X || A320
    this->window = SDL::window (title, SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED, width, height, window_flags);
    if (this->window == 0) {
        throw std::runtime_error (SDL_GetError ());
    }
    int window_width = width;
    int window_height = height;
    SDL_GetWindowSize (this->window.get (), &window_width, &window_height);
    this->scale = std::max(1,
            std::min (window_width / width, window_height / height));

    if (fill_policy == FILL_BOTH || fill_policy == FILL_HEIGHT) {
        this->height = window_height / this->scale;
    } else {
        this->height = height;
    }
    if (fill_policy == FILL_BOTH || fill_policy == FILL_WIDTH) {
        this->width = window_width / this->scale;
    } else {
        this->width = width;
    }
    this->half_height = this->height / 2;
    this->half_width = this->width / 2;

    this->translation.x = window_width / 2 - this->half_width * this->scale;
    this->translation.y = window_height / 2 - this->half_height * this->scale;

    this->renderer = SDL::renderer (this->window, -1, SDL_RENDERER_ACCELERATED);
    if (this->renderer == 0) {
        throw std::runtime_error (SDL_GetError ());
    }
}

void Screen::beginRender () {
    SDL_SetRenderDrawColor (this->renderer.get (), this->color.r, this->color.g,
            this->color.b, SDL_ALPHA_OPAQUE);
    SDL_RenderClear (this->renderer.get ());
#if !defined(EMSCRIPTEN)
    glPushMatrix ();
    glTranslatef (this->translation.x, this->translation.y, 0.0);
    glScalef (this->scale, this->scale, 1.0);
#endif // !EMSCRIPTEN
}

void Screen::endRender () {
    SDL_RenderPresent (this->renderer.get ());
#if !defined(EMSCRIPTEN)
    glPopMatrix ();
#endif // !EMSCRIPTEN
}
