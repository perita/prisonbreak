//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TEXT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TEXT_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <boost/shared_ptr.hpp>
#include "Texture.hpp"
#include "../BitmapFont.hpp"

namespace benzaiten {
    ///
    /// @class Text
    /// @brief Texture that shows a text message.
    ///
    class Text: public benzaiten::Texture {
        public:
            /// A pointer to a text.
            typedef boost::shared_ptr<Text> Ptr;

            ///
            /// @brief Creates a new Text with a font and an initial text.
            ///
            /// @param[in] font The font to use to draw the text.
            /// @param[in] text The text string to show.
            ///
            static Ptr New (const BitmapFont &font, const std::string &text = "") {
                return Ptr (new Text (font, text)); 
            }

            virtual void centerOrigin ();

            ///
            /// @brief Sets the text to show.
            ///
            /// The new text is shown on the next update.
            ///
            /// @param[in] text The text to show.
            ///
            void setText (const std::string &text);

        protected:
            ///
            /// @brief Creates a new Text with a font and an initial text.
            ///
            /// @param[in] font The font to use to draw the text.
            /// @param[in] text The text string to show.
            ///
            Text (const BitmapFont &font, const std::string &text = "");

        private:
            /// Tells whether to center the origin when updating the text.
            bool center_origin;
            /// The font to use to render the text.
            BitmapFont font;
            /// The text to show.
            std::string text;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_TEXT_HPP
