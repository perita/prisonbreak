//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GRAPHICS_GRAPHIC_LIST_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GRAPHICS_GRAPHIC_LIST_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <boost/shared_ptr.hpp>
#include "../Graphic.hpp"

namespace benzaiten {
    ///
    /// @class GraphicList
    /// @brief A list of Graphic objects.
    ///
    class GraphicList: public Graphic {
        public:
            /// A pointer to a GraphicList instance.
            typedef boost::shared_ptr<GraphicList> Ptr;

            ///
            /// @brief Creates an empty GraphicList
            ///
            /// @return The instance of the new GraphicList
            ///
            static Ptr New () { return Ptr (new GraphicList ()); }

            ///
            /// @brief Adds a new Graphic to the list.
            ///
            /// @param[in] graphic The Graphic to add to the list.
            ///
            void add (const Graphic::Ptr &graphic);

            virtual void render (SDL_Renderer &renderer, int x, int y) const;
            virtual void update (double elapsed);

        protected:
            ///
            /// @brief Constructs an empty GraphicList.
            ///
            GraphicList ();

        private:
            /// The list of graphics.
            std::list<Graphic::Ptr> graphics;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GRAPHIC_GRAPHIC_LIST_HPP
