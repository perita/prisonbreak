//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <map>
#include <vector>
#include <SDL_joystick.h>
#include <SDL_keycode.h>
#include <SDL_events.h>

#if defined (A320)
#include "a320.hpp"
#endif // A320
#if defined (GP2X)
#include "gp2x.hpp"
#endif // GP2X

namespace benzaiten {

    ///
    /// @strucy Action
    /// @brief Holds the enumeration of benzaiten actions.
    ///
    struct Action {
        enum {
            /// Any key.
            ANY = -1,
            /// Accept a menu selection.
            MENU_ACCEPT = -2,
            /// Cancel button in menu.
            MENU_CANCEL = -3,
            /// Menu selection down.
            MENU_DOWN = -4,
            /// Menu selection left.
            MENU_LEFT = -5,
            /// Menu selection right.
            MENU_RIGHT = -6,
            /// Menu selection up.
            MENU_UP = -7
        };
    };

    ///
    /// @enum MenuAction
    /// @brief The menu's actions.
    ///
    enum MenuAction {
        Cancel,
        Select,
        Up,
        Down,
        Left,
        Right
    };

    ///
    /// @class InputManager
    /// @brief The manager for game events.
    ///
    class InputManager {
        public:
            ///
            /// @brief Initializes the joystick.
            ///
            InputManager ();

            ///
            /// @brief Uninitializes the joystick.
            ///
            ~InputManager ();

            ///
            /// @brief Handles a joystick axis.
            ///
            /// @param[in] event The axis event data.
            ///
            void handleAxis (const SDL_JoyAxisEvent &event);

            ///
            /// @brief Handles a joystick buttons.
            ///
            /// @param[in] event The button event data.
            ///
            void handleButton (const SDL_JoyButtonEvent &event);

            ///
            /// @brief Handles a key event.
            ///
            /// @param[in] event The keyboard event data.
            ///
            void handleKeyboard (const SDL_KeyboardEvent &event);

            ///
            /// @brief Checks if an action is hold down.
            ///
            /// @param[in] action The action to check.
            ///
            /// @return @c true if the @p action is hold down.
            ///
            bool isDown (int action) const;

            ///
            /// @brief Maps actions to the joystick X axis.
            ///
            /// @param[in] left_action The action to map to the left
            ///            position.
            /// @param[in] right_action The action to map to the right
            ///            position
            ///
            void mapAxisX(int left_action, int right_action);

            ///
            /// @brief Maps actions to the joystick Y axis.
            ///
            /// @param[in] up_action The action to map to the up
            ///            position.
            /// @param[in] down_action The action to map to the down
            ///            position
            ///
            void mapAxisY(int up_action, int down_action);

            ///
            /// @brief Maps an action to a list of buttons.
            ///
            /// @param[in] action The action to map the keys to.
            /// @param[in] buttons The list of buttons to map.
            ///
            void mapButtons (int action, const std::vector<Uint8> &buttons);

            ///
            /// @brief Maps an action to a list of keys.
            ///
            /// @param[in] action The action to map the keys to.
            /// @param[in] keys The list of key symbols to map.
            ///
            void mapKeys (int action, const std::vector<SDL_Keycode> &keys);

            ///
            /// @brief Checks if an action has been pressed this frame.
            ///
            /// @param[in] action The action to check.
            ///
            /// @return @c true if the @p action has been pressed this frame.
            ///
            bool pressed (int action) const;

            ///
            /// @brief Checks if an action has been released this frame.
            ///
            /// @param[in] action The action to check.
            ///
            /// @return @c true if the @p action has been released this
            ///         frame.
            ///
            bool released (int action) const;

            ///
            /// @brief Fetches and processes all enqueued events.
            ///
            void update ();

            /// Whether the mouse button is down.
            bool mouse_down;
            /// Whether the mouse button has been pressed this frame.
            bool mouse_pressed;
            /// Whether the mouse button has been released this frame.
            bool mouse_released;
            /// The current mouse's X position.
            int mouse_x;
            /// The current mouse's Y position.
            int mouse_y;

        private:
    ///
    /// @enum Axis
    /// @brief The joystick's axis.
    ///
    enum Axis {
        AxisNone,
        AxisDown,
        AxisLeft,
        AxisRight,
        AxisUp
    };

            ///
            /// @struct State
            /// @brief The state of an action.
            ///
            struct State {
                /// Whether the action is down.
                bool down;
                /// Whether the action has been pressed in the last frame.
                bool pressed;
                /// Whether the action has been released in the last frame.
                bool released;

                ///
                /// @brief Constructor.
                ///
                State ()
                    : down (false)
                    , pressed (false)
                    , released (false)
                {
                }
            };

            /// The map of states.
            typedef std::map<int, State> StateMap;
            /// The type of a joy axis map.
            typedef std::map<Axis, std::vector<int> > AxisMap;
            /// The map of a buttons and actions.
            typedef std::map<Uint8, std::vector<int> > ButtonMap;
            /// The map of keys and actions.
            typedef std::map<SDL_Keycode, std::vector<int> > KeyMap;

            ///
            /// @brief Sets the new Axis for X.
            ///
            /// @param[in] axis The axis to set to X.
            ///
            void setAxisX (Axis axis);

            ///
            /// @brief Sets the new Axis for Y.
            ///
            /// @param[in] axis The axis to set to Y.
            ///
            void setAxisY (Axis axis);

            /// The state of all actions.
            StateMap actions_state;
            /// The mapping between a joystick axis and an action.
            AxisMap axis_map;
            /// The current axis on the X.
            Axis axis_x;
            /// The current axis on the Y.
            Axis axis_y;
            /// The mapping between a joystick button and an action.
            ButtonMap button_map;
            /// The joystick.
            SDL_Joystick *joystick;
            /// The mapping between a key and an action.
            KeyMap key_map;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP
