//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GRAPHICS_TILEMAP_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GRAPHICS_TILEMAP_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/multi_array.hpp>
#include <boost/shared_ptr.hpp>
#include "../Graphic.hpp"
#include "../SDL.hpp"

namespace benzaiten {
    ///
    /// @class TileMap
    /// @brief Uses a texture as titles to draw a bigger map.
    ///
    /// The tile map evenly divides a texture in cells of the same size and
    /// numerates them from right to left, up to bottom, starting at 0.  It
    /// also has another bi-dimensional array where each cell is an index to
    /// a cell of the texture's grid.
    ///
    /// This can be used to draw bigger maps using only a handful of
    /// different graphics.  All of them, however, must be in the same
    /// texture and have the same size.
    ///
    class TileMap: public Graphic {
        public:
            /// A smart pointer to a TileMap.
            typedef boost::shared_ptr<TileMap> Ptr;

            ///
            /// @brief Creates a new TileMap with an empty grid.
            ///
            /// @param[in] tiles The texture to use to draw the map's
            ///            individual tiles.
            /// @param[in] columns The total width, in tiles, of the map.
            /// @param[in] rows The total height, in tiles, of the map.
            /// @param[in] tile_width The width, in pixels, of each
            ///            individual tile.
            /// @param[in] tile_height The height, in pixels, of each
            ///            individual tile.
            ///
            /// @return A smart pointer to the newly created TileMap.
            ///
            static Ptr New (const SDL::Texture &tiles,
                    unsigned int columns, unsigned int rows, int tile_width,
                    int tile_height) {
                return Ptr (new TileMap (tiles, columns, rows, tile_width,
                            tile_height));
            }

            ///
            /// @brief Gets the columns of the map.
            ///
            /// @return The width, in tiles, of the map.
            ///
            unsigned int columns () const { return this->map.shape ()[1]; }

            ///
            /// @brief Gets the index of a tile.
            ///
            /// @param row The row of the tile to get the index of.  The
            ///        accepted range is from 0 to tilesRows().
            /// @param column The column of the tile to get the index of.
            ///        The accepted range is from 0 to tilesColumns().
            int getIndex (int row, int column) const {
                return row * this->tilesColumns () + column;
            }

            virtual void render (SDL_Renderer &renderer, int x, int y) const;

            ///
            /// @brief Gets the number of rows of the map.
            ///
            /// @return The height, in tiles, of the map.
            ///
            unsigned int rows () const { return this->map.shape ()[0]; }

            ///
            /// @brief Sets the index of a map's cell.
            ///
            /// @param[in] x The X position of the cell to set.
            /// @param[in] y The Y position of the cell to set.
            /// @param[in] index The index to set at cell (x, y).
            ///
            void setIndex (unsigned int x, unsigned int y,
                    int index);

            ///
            /// @brief The number of columns of tiles available.
            ///
            /// @return The number of columns of tiles.
            ///
            int tilesColumns () const { return this->tiles_cols; }

            ///
            /// @brief The number of rows of tiles available.
            ///
            /// @return The number of rows of tiles.
            ///
            int tilesRows () const { return this->tiles_rows; }

        protected:
            ///
            /// @brief Constructor of a TileMap with an empty grid.
            ///
            /// @param[in] tiles The texture to use to draw the map's
            ///            individual tiles.
            /// @param[in] columns The total width, in tiles, of the map.
            /// @param[in] rows The total height, in tiles, of the map.
            /// @param[in] tile_width The width, in pixels, of each
            ///            individual tile.
            /// @param[in] tile_height The height, in pixels, of each
            ///            individual tile.
            ///
            /// @return A smart pointer to the newly created TileMap.
            ///
            TileMap (const SDL::Texture &tiles, unsigned int columns,
                    unsigned int rows, int tile_width, int tile_height);

        private:
            /// The type of Map.
            typedef boost::multi_array<int, 2> Map;
            /// The index of a Map.
            typedef Map::index MapIndex;

            /// The grid with all the tiles' index.
            Map map;
            /// The rectangle of a tile.
            SDL_Rect tile;
            /// The texture with all the tiles.
            SDL::Texture tiles;
            /// The number of columns of tiles available.
            int tiles_cols;
            /// The number of rows of tiles available.
            int tiles_rows;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GRAPHICS_TILEMAP_HPP
