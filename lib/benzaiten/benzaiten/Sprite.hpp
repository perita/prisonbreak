//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <SDL_rect.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "Graphic.hpp"
#include "Point2D.hpp"

// Forward declarations
class SDL_Renderer;

namespace benzaiten {
    // Forward declarations
    class Context;

    ///
    /// @class Sprite
    /// @brief The most basic entity of a game.
    ///
    /// A sprite is an entity that must be placed in a Context and that can be
    /// used to show an image, interact with the input or simply any active
    /// element (visible or not) in the game.
    ///
    class Sprite: boost::noncopyable {
        public:
            /// An smart pointer to a Sprite.
            typedef boost::shared_ptr<Sprite> Ptr;
            /// A list of types.
            typedef std::vector<int> TypeList;

            ///
            /// @brief Creates a new Sprite with a position and graphic.
            ///
            /// @param[in] x The initial X position of the Sprite.
            /// @param[in] y The initial Y position of the Sprite.
            /// @param[in] graphic The graphic to assign to the Sprite.  The Sprite
            ///            assumes the ownership of this graphic.
            ///
            static Ptr New (double x = 0.0, double y = 0.0,
                    const Graphic::Ptr &graphic = Graphic::Ptr()) {
                return Ptr (new Sprite (x, y, graphic));
            }

            ///
            /// @brief Advances the Sprite by an amount.
            ///
            /// @param[in] x The amount to advance in x.
            /// @param[in] y The amount to advance in y.
            /// @param[in] solid The list of sprite types that are considered
            ///            solid.  If when advancing the sprite collides
            ///            with any other sprite of this type, then the
            ///            advanceCollideX() and/or advanceCollideY()
            ///            functions are called.  If the list is empty, no
            ///            check is done.
            ///
            void advance (double x, double y,
                    const TypeList &solid = TypeList ());

            ///
            /// @brief Checks whether this Sprite collides with a point.
            ///
            /// @param[in] point The point to check if collides with this
            ///            sprite.
            ///
            /// @return @c true if this sprite collides with @p point
            ///         at the sprite's current position.
            ///
            bool collides (const Point2D<double> &point) const {
                return this->collides (this->x(), this->y(), point);
            }

            ///
            ///
            /// @brief Checks whether this Sprite collides with a point.
            ///
            /// @param[in] x The X position where to check if the sprite
            ///            collide
            /// @param[in] y The Y position where to check if the sprite
            ///            collide
            /// @param[in] point The point to check if collides with this
            ///            sprite.
            ///
            /// @return @c true if this sprite collides with @p point
            ///         at position @p x, @p y.
            bool collides (double x, double y,
                    const Point2D<double> &point) const;

            ///
            /// @brief Checks whether this Sprites collides with another.
            ///
            /// This checks if this sprite, at the specified position,
            /// collides with another sprite.
            ///
            /// @param[in] x The X position where to check if the sprites
            ///            collide
            /// @param[in] y The Y position where to check if the sprites
            ///            collide
            /// @param[in] other The other sprite to check if collides
            ///            with this.
            ///
            /// @return @c true if this sprite collides with @p other
            ///         at position @p x, @p y.
            ///
            bool collides (double x, double y, const Sprite &other) const;

            ///
            /// @brief Checks collision with a sprite type.
            ///
            /// Checks whether this sprite collides with any of the sprites
            /// in the same context that are of the specified type.
            ///
            /// @param[in] x The X position where to check if the sprites
            ///            collide.
            /// @param[in] y The Y position where to check if the sprites
            ///            collide.
            /// @param[in] type The type of sprite to check if this sprite
            ///            collides with.
            ///
            /// @return The sprite which collided with this sprite, or
            ///         @c NULL if no sprite collided.
            ///
            Sprite *collides (double x, double y, int type) const;

            ///
            /// @brief Checks collision with a list of sprite types.
            ///
            /// Checks whether this sprite collides with any of the sprites
            /// in the same context that are of any of the types specified
            /// at the given position.
            ///
            /// @param[in] x The X position where to check if the sprites
            ///            collide.
            /// @param[in] y The Y position where to check if the sprites
            ///            collide.
            /// @param[in] types The list of types to check if this sprite
            ///            collides with.
            ///
            /// @return The sprite which collided with this sprite, or
            ///         @c NULL if no sprite collided.
            ///
            Sprite *collides (double x, double y, const TypeList &types) const;

            ///
            /// @brief Computes the squared distance to another Sprite.
            ///
            /// @param[in] other The other sprite to compute the squared
            ///            distance to.
            ///
            /// @return The squared distance between this sprite and the
            ///         other.
            ///
            double distanceSquaredTo (const Sprite::Ptr &other) const;

            ///
            /// @brief Computes the squared distance to another Sprite.
            ///
            /// @param[in] other The other sprite to compute the squared
            ///            distance to.
            ///
            /// @return The squared distance between this sprite and the
            ///         other.
            ///
            double distanceSquaredTo (const Sprite &other) const;

            ///
            /// @brief Gets the sprite's graphic.
            ///
            /// @return The pointer to the sprite's graphic.
            ///
            Graphic::Ptr getGraphic () const { return this->graphic; }

            ///
            /// @brief Gets the sprite's height.
            ///
            /// @return The sprite's height in pixels.
            ///
            int height () const { return this->hitbox.h; }

            ///
            /// @brief Tells whether the sprite is active.
            ///
            /// @return @c true if the sprite is active.
            ///
            bool isActive () const { return this->active; }

            ///
            /// @brief Moves the Sprite to the specified position.
            ///
            /// @param[in] x The X position to move the sprite to.
            /// @param[in] y The Y position to move the sprite to.
            ///
            void moveTo (double x, double y);

            ///
            /// @brief Renders the Sprite onto the renderer.
            ///
            /// @param[inout] renderer The renderer to use to render the Sprite.
            ///
            virtual void render (SDL_Renderer &renderer) const;

            ///
            /// @brief Updates the Sprite's state.
            ///
            /// @param[in] elapsed The seconds elapsed since the last call to this
            ///            function.
            ///
            virtual void update (double elapsed) { }

            ///
            /// @brief Sets whether the Sprite is active.
            ///
            /// @param[in] active Set to @c true to set the Sprite active.
            ///
            void setActive (bool active) { this->active = active; }

            ///
            /// @brief Sets the Graphic for this Sprite.
            ///
            /// @param[in] graphic The Graphic to set to this Sprite.  The
            ///            Sprite assumes ownership of this Graphic.
            ///
            void setGraphic (const Graphic::Ptr &graphic);

            ///
            /// @brief Sets the Sprite's hitbox.
            ///
            /// @param[in] x The origin in X of the hit box.
            /// @param[in] y The origin in Y of the hit box.
            /// @param[in] width The hitbox's width.
            /// @param[in] height The hitbox's height.
            ///
            void setHitbox (int x, int y, int width, int height);

            ///
            /// @brief Sets the Layer this Sprite belongs to.
            ///
            /// @param[in] layer The layer this Graphic belongs to.
            ///
            void setLayer (int layer);

            ///
            /// @brief The sprite's width.
            ///
            /// @param The sprite's width in pixels.
            ///
            int width () const { return this->hitbox.w; }

            ///
            /// @brief Gets the Sprite's X position.
            ///
            /// @return The sprite's X position.
            ///
            double x () const { return this->position.x; }

            ///
            /// @brief Sets the Sprite's X position.
            ///
            /// @param[in] x The new sprite's X position.
            ///
            void x (double x) { this->position.x = x; }

            ///
            /// @brief Gets the Sprite's Y position.
            ///
            /// @return The sprite's Y position.
            ///
            double y () const { return this->position.y; }

            ///
            /// @brief Sets the Sprite's Y position.
            ///
            /// @param[in] y The new sprite's X position.
            ///
            void y (double y) { this->position.y = y; }

        protected:
            ///
            /// @brief Creates a new Sprite with a position and graphic.
            ///
            /// @param[in] x The initial X position of the Sprite.
            /// @param[in] y The initial Y position of the Sprite.
            /// @param[in] graphic The graphic to assign to the Sprite.  The
            ///            Sprite assumes the ownership of this graphic.
            ///
            explicit Sprite(double x = 0.0, double y = 0.0,
                    const Graphic::Ptr &graphic = Graphic::Ptr());

            ///
            /// @brief Called when collided while advancing in X.
            ///
            /// @param[in] sprite The reference to the sprite that collided
            ///            with this when advancing in the X direction.
            ///
            virtual void advanceCollideX (Sprite &sprite) { }

            ///
            /// @brief Called when collided while advancing in Y.
            ///
            /// @param[in] sprite The reference to the sprite that collided
            ///            with this when advancing in the Y direction.
            ///
            virtual void advanceCollideY (Sprite &sprite) { }

            /// The context that holds this sprite.
            Context *context;
            /// The Sprite's graphic.
            Graphic::Ptr graphic;
            /// The type that this Sprite belongs to.
            int type;

        private:
            // To allow access to the next and previous pointers.
            friend class Context;
            template<class> friend class SpriteTypeIterator;

            /// The sprite is active.
            bool active;
            /// The remaining amount to advance a full integer.
            Point2D<double> advance_rest;
            /// The layer this Sprite is set to.
            int layer;
            /// The hitbox to use in collisions.
            SDL_Rect hitbox;
            /// The sprite's position.
            Point2D<double> position;
            /// The next Sprite to render after this one.
            Sprite *render_next;
            /// The Sprite to render prior to this.
            Sprite *render_prev;
            /// The next Sprite in type.
            Sprite *type_next;
            /// The Sprite in type prior to this.
            Sprite *type_prev;
            // The following is the only smart pointer because it is the only
            // place that I can be sure that all the other are linked.
            // However, I can only have one of these because otherwise I could
            // create loops and memory leaks.
            /// The next Sprite to update after this one.
            Sprite::Ptr update_next;
            /// The Sprite to update prior to this.
            Sprite *update_prev;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP
