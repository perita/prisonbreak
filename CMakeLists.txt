# I've only tested this project with CMake 3.0.
# So, for now, this is the required version.
cmake_minimum_required(VERSION 3.0)

# The project's name.  This will be the name of the solution file for
# Microsoft Visual Studio.
project(PrisonBreak)

# Where to find Benzaiten's CMake macros.
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/lib/benzaiten/cmake)

# The application's version.
include(SetVersion)
SET_VERSION(1 2 0)

# Process subdirectories.
add_subdirectory(assets)
add_subdirectory(lib)
add_subdirectory(src)

# Create the WiX source file and the MSI target.
include(MsiTarget)
ADD_BENZAITEN_MSI(prisonbreak "-${VERSION}")
