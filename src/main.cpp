//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include <cstdlib>
#include <stdexcept>
#include <benzaiten/BitmapFont.hpp>
#include <benzaiten/ErrorMessage.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/contexts/Logo.hpp>
#include <boost/assign.hpp>
#include "contexts/Escape.hpp"

namespace prisonbreak {
    class Main: public benzaiten::Game {
        public:
            Main ():
                Game (240, 400, "Prison Break", "prisonbreak", true)
            {
                this->setScreenFillPolicy (benzaiten::Screen::FILL_BOTH);
            }

        protected:
            virtual void init () {
                using benzaiten::BitmapFont;
                using benzaiten::contexts::Logo;
                using namespace boost::assign;

                Game::input ().mapKeys (
                        Actions::DOWN, list_of<int>(SDLK_DOWN)(SDLK_j));
                Game::input ().mapKeys (
                        Actions::LEFT, list_of<int>(SDLK_LEFT)(SDLK_h));
                Game::input ().mapKeys (
                        Actions::RIGHT, list_of<int>(SDLK_RIGHT)(SDLK_l));
                Game::input ().mapKeys (
                        Actions::UP, list_of<int>(SDLK_UP)(SDLK_k));
                Game::input ().mapAxisX (Actions::LEFT, Actions::RIGHT);
                Game::input ().mapAxisY (Actions::UP, Actions::DOWN);
                Game::input ().mapKeys (Actions::MUTE, list_of<int>(SDLK_m));

                BitmapFont font (
                        " 0123456789ACDEHKLMNOPRSTUY",
                        resources().surface ("font.tga"));
                switchTo (Logo::New ("logo.tga", Escape::New (font)));
            }
    };
}

int main (int argc, char *argv[]) {
    prisonbreak::Main game;
    try {
        game ();
        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        benzaiten::showErrorMessage (e.what ());
    } catch (...) {
        benzaiten::showUnknownErrorMessage ();
    }
    return EXIT_FAILURE;
}
