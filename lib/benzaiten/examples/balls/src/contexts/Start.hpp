//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_START_HPP)
#define GEISHA_STUDIOS_START_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Context.hpp>
#include <boost/shared_ptr.hpp>

///
/// @class Start
/// @brief Shows a text before the Level starts.
///
/// This Context is merely a way to tell the player to tap the screen
/// to start the level or to repeat the same level is she didn't
/// reach the target criteria on the pervious run.
///
class Start: public benzaiten::Context {
    public:
        /// A smart pointer to a Start context.
        typedef boost::shared_ptr<Start> Ptr;

        ///
        /// @brief Creates a smart pointer to a Start context.
        ///
        /// @param[in] level This is the level that should be played next
        ///            when this context is over.  It should be the first
        ///            level the first time this context is created and then
        ///            set it to the next level to continue or the same
        ///            to repeat the same.
        /// @param[in] next Tells whether @level is the next level or
        ///            the same.  This is used by the context to know
        ///            which message show to the user: try again, or 
        ///            continue to the next level.
        ///
        /// @return The smart pointer to the newly created Start context.
        ///
        static Ptr New (unsigned int level, bool next) {
            return Ptr (new Start (level, next));
        }

        virtual void update (double elapsed);

    protected:
        ///
        /// @brief Constructor
        ///
        /// @param[in] level This is the level that should be played next
        ///            when this context is over.  It should be the first
        ///            level the first time this context is created and then
        ///            set it to the next level to continue or the same
        ///            to repeat the same.
        /// @param[in] next Tells whether @level is the next level or
        ///            the same.  This is used by the context to know
        ///            which message show to the user: try again, or 
        ///            continue to the next level.
        ///
        Start (unsigned int level, bool next);

    private:
        ///
        /// @brief Gets the context of the Level to switch next.
        ///
        /// When this context is over (i.e., the user pressed a mouse
        /// button or tapped the screen) it has to switch to a new
        /// Context.  This function returns the appropriate context
        /// to switch to based on the current level.
        ///
        /// @return The smart pointer to the level to switch to.
        ///
        Context::Ptr levelToSwitch ();

        /// The level to switch to next.
        unsigned int level;
};

#endif // !GEISHA_STUDIOS_START_HPP

