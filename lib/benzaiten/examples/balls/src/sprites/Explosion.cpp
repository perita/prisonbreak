//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Explosion.hpp"
#include <benzaiten/Context.hpp>
#include <benzaiten/Primitives.hpp>

namespace {
    /// The speed in pixels/second that the Explosion grows and shrinks.
    const double SPEED = 45.0;
    /// The maximum radius in pixels that the Explosion can have.
    const double MAX_RADIUS = 50.0;
}

Explosion::Explosion (int x, int y, const SDL_Color &color, double radius):
    Sprite (x, y),
    color (color),
    dir (SPEED),
    radius (radius)
{
    // Sets the type of this Sprite to find it later using
    // Context::firstOfType ().  Type::EXPLOSION is defined
    // in config.h.cmake but could be any integer.
    this->type = Type::EXPLOSION;
}

void Explosion::render (SDL_Renderer &renderer) const {
    using benzaiten::renderFillCircle;

    // The Explosion is simply a filled circle of the given color and
    // the current radius.
    renderFillCircle (renderer, this->x (), this->y (), this->radius,
            this->color);
}

void Explosion::update (double elapsed) {
    // Grow the Explosion's radius until it reached the maximum radius
    // allowed for an Explosion.  Then, reverse the growth direction
    // as to shrink the explosion until its radius is zero and remove
    // itself from the context it belongs to.
    this->radius += this->dir * elapsed;
    if (this->dir > 0 && this->radius > MAX_RADIUS) {
        this->dir *= -1;
    }
    if (radius < 1.0) {
        // When added to a context with Context::add() the context
        // sets this variable--`context`--that points to itself.
        this->context->remove (this);
    }
}
