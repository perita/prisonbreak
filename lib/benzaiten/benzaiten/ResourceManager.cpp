//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "ResourceManager.hpp"
#include <tinyxml.h>
#include <boost/assign.hpp>
#include "MusicNull.hpp"
#include "LoadTGA.hpp"
#include "SoundNull.hpp"

#if defined (WIN32)
#include <windows.h>
#endif // WIN32

#if defined (UNIX) && !defined (APPLE) && !defined (GP2X) && !defined(ANDROID) && !defined(EMSCRIPTEN)
#include <binreloc.h>
#endif // UNIX && !APPLE && !GP2X

using namespace benzaiten;

namespace {
    ///
    /// @brief Checks that the audio subsystem is enabled.
    ///
    /// @throw std::invalid_argument is the audio subsystem is not enabled.
    ///
    void checkAudioIsEnabled () {
        int frequency;
        int channels;
        uint16_t format;
        if (0 == Mix_QuerySpec (&frequency, &format, &channels)) {
            throw std::invalid_argument ("Can't open music without the audio system.");
        }
    }
}

ResourceManager::ResourceManager (const std::string &game_id,
        SDL_Renderer &renderer)
    : musics ()
    , renderer (renderer)
    , resources_path (findResourcesPath (game_id))
    , sounds ()
    , textures ()
{
}

std::string ResourceManager::findResourcesPath (const std::string &game_id) {
#if defined (APPLE) || defined (GP2X) || defined (ANDROID)
    // In Mac OS X we assume that the application's working directory is
    // the Resources directory, so we use the current directory as the
    // resource's path.
    return std::string (".");
#elif defined (EMSCRIPTEN)
    return std::string ("assets");
#elif defined (UNIX)
#   if defined PACKAGE_DATA_DIR
    std::string resources_path (PACKAGE_DATA_DIR);
#   else // !PACKAGE_DATA_DIR
    std::string resources_path ("/usr/share");
#   endif // PACKAGE_DATA_DIR

    BrInitError error;
    if (br_init (&error) == 0 && BR_INIT_ERROR_DISABLED != error) {
        // If binary relocation is disabled just return the hard coded path.
        // in resources_path.
    } else {
        resources_path = br_find_data_dir (resources_path.c_str ());
    }
    return resources_path + "/" + game_id;
#elif defined (WIN32)
    // Initially assume that we are running in the executable's working path.
    std::string resources_path (".");

    // Now get the full path to the executable.
    TCHAR executable_file[MAX_PATH];
    if (GetModuleFileName (0, executable_file, MAX_PATH) > 0) {
        std::string executable_path (executable_file);
        std::string::size_type backslash_pos = executable_path.rfind ("\\");
        if (std::string::npos != backslash_pos) {
            // If we can find it, append the relative resources path
            // to the absolute executable's path to make it an absolute
            // path (more or less... probably it will fail if we get
            // past the MAX_PATH limit somewhere.)
            executable_path.erase (backslash_pos);
            resources_path = executable_path + "\\" + resources_path;
        }
    }

    return resources_path;
#else
#error Unknown platform.
#endif
}

std::string ResourceManager::getFilePath (const std::string &directory,
        const std::string &file_name) const {
#if defined (ANDROID)
    return joinPath (directory, file_name);
#else
    return joinPath (joinPath (this->resources_path, directory), file_name);
#endif
}

std::string ResourceManager::joinPath (const std::string &path0,
        const std::string &path1) {
#if defined (WIN32)
    const char separator = '\\';
#else // !WIN32
    const char separator = '/';
#endif // WIN32

    return path0 + separator + path1;
}

TextureAtlas::Ptr ResourceManager::atlas (const std::string &file_name) const {
    return this->loadTextureAtlasFile (this->getFilePath ("gfx", file_name));
}

SDL::Music ResourceManager::loadMusicFile (const std::string &file_name) {
    checkAudioIsEnabled ();

    SDL::Music music (SDL::music(file_name));
    if (!music) {
        throw std::invalid_argument (std::string ("Error loading music ") +
                file_name + ": " + Mix_GetError ());
    }
    return music;
}

SDL::Sound ResourceManager::loadSoundFile (
        const std::string &file_name) {
    checkAudioIsEnabled ();

    SDL::Sound sound (SDL::sound (file_name.c_str ()));
    if (!sound) {
        throw std::invalid_argument (std::string ("Error loading sound ") +
                file_name + ": " + Mix_GetError ());
    }
    return sound;
}

SDL::Surface ResourceManager::loadSurfaceFile (
        const std::string &file_name) {
    SDL::Surface surface (loadTGA (file_name));

    // Set the color key only if there is no alpha channel; otherwise use
    // alpha as transparency.
    bool hasAlpha = 32 == surface->format->BitsPerPixel;
    if (!hasAlpha) {
        Uint32 transparent_color = SDL_MapRGB (surface->format, 255, 0, 255);
        SDL_SetColorKey (surface.get (), SDL_TRUE, transparent_color);
    }

    return surface;
}

SDL::Texture ResourceManager::loadTextureFile (const std::string &file_name,
        SDL_Renderer &renderer) {
    return SDL::texture (&renderer, loadSurfaceFile (file_name));
}

TextureAtlas::Ptr ResourceManager::loadTextureAtlasFile (
        const std::string &file_name) const {
    TiXmlDocument xml (file_name);
    if (!xml.LoadFile ()) {
        throw std::invalid_argument ("Couldn't load texture atlas `" + file_name + "'");
    }

    TiXmlNode *child = xml.FirstChild ();
    if (child->Type () != TiXmlNode::TINYXML_DECLARATION) {
        throw std::invalid_argument ("Atlas file `" + file_name + "' has no XML declaration");
    }
    child = child->NextSiblingElement ();
    if (child == 0) {
        throw std::invalid_argument ("Reached end of input while reading atlas file `" + file_name + "'");
    }

    if (std::string ("map") != child->Value ()) {
        throw std::invalid_argument ("Atlas file `" + file_name + "' doesn't start with <map>");
    }
    std::string image;
    {
        TiXmlElement *map = child->ToElement ();
        if (map->QueryStringAttribute ("image", &image) != TIXML_SUCCESS) {
            throw std::invalid_argument ("Atlas file `" + file_name + "' doesn't have an image file");
        }
    }

    TextureAtlas::Ptr atlas = TextureAtlas::New (this->texture (image));
    for (child = child->FirstChild (); child != 0 ; child = child->NextSibling ()) {
        if (std::string ("image") == child->Value ()) {
            using namespace boost::assign;

            TiXmlElement *image = child->ToElement ();
            std::string name;
            int x;
            int y;
            int width;
            int height;
            if (image->QueryStringAttribute ("name", &name) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Image tag has no name in `" + file_name + "'");
            }
            if (image->QueryIntAttribute ("x", &x) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Image tag has no x in `" + file_name + "'");
            }
            if (image->QueryIntAttribute ("y", &y) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Image tag has no y in `" + file_name + "'");
            }
            if (image->QueryIntAttribute ("width", &width) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Image tag has no width in `" + file_name + "'");
            }
            if (image->QueryIntAttribute ("height", &height) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Image tag has no height in `" + file_name + "'");
            }
            atlas->add (name, list_of(atlas->newFrame (x, y, width, height)), 0.0, false);
        } else if (std::string ("animation") == child->Value ()) {
            std::string name;
            double rate;
            bool loop;
            std::vector<unsigned int> frames;

            TiXmlElement *animation = child->ToElement ();
            if (animation->QueryStringAttribute ("name", &name) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Animation tag has no name in `" + file_name + "'");
            }
            if (animation->QueryDoubleAttribute ("rate", &rate) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Animation tag has no rate in `" + file_name + "'");
            }
            if (animation->QueryBoolAttribute ("loop", &loop) != TIXML_SUCCESS) {
                throw std::invalid_argument ("Animation tag has no loop in `" + file_name + "'");
            }
            for (TiXmlNode *frame_node = child->FirstChild () ; frame_node != 0 ;
                    frame_node = frame_node->NextSibling ()) {
                if (std::string ("frame") == frame_node->Value ()) {
                    int x;
                    int y;
                    int width;
                    int height;
                    TiXmlElement *frame = frame_node->ToElement ();
                    if (frame->QueryIntAttribute ("x", &x) != TIXML_SUCCESS) {
                        throw std::invalid_argument ("Image tag has no x in `" + file_name + "'");
                    }
                    if (frame->QueryIntAttribute ("y", &y) != TIXML_SUCCESS) {
                        throw std::invalid_argument ("Image tag has no y in `" + file_name + "'");
                    }
                    if (frame->QueryIntAttribute ("width", &width) != TIXML_SUCCESS) {
                        throw std::invalid_argument ("Image tag has no width in `" + file_name + "'");
                    }
                    if (frame->QueryIntAttribute ("height", &height) != TIXML_SUCCESS) {
                        throw std::invalid_argument ("Image tag has no height in `" + file_name + "'");
                    }
                    frames.push_back (atlas->newFrame (x, y, width, height));
                }
            }
            atlas->add (name, frames, rate, loop);
        }
    }
    return atlas;
}

SDL::Music ResourceManager::music (const std::string &file_name) const {
    MusicRes music (this->musics[file_name]);
    SDL::Music music_ptr (music.lock ());
    if (!music_ptr) {
        music_ptr = this->loadMusicFile (this->getFilePath ("music", file_name));
        this->musics[file_name] = music_ptr;
    }
    return music_ptr;
}

SDL::Sound ResourceManager::sound (const std::string &file_name) const {
    SoundRes sound (this->sounds[file_name]);
    SDL::Sound sound_ptr (sound.lock ());
    if (!sound_ptr) {
        sound_ptr = this->loadSoundFile (this->getFilePath ("sfx", file_name));
        this->sounds[file_name] = sound_ptr;
    }
    return sound_ptr;
}

SDL::Surface ResourceManager::surface (const std::string &file_name) const {
    SurfaceRes surface (this->surfaces[file_name]);
    SDL::Surface surface_ptr (surface.lock ());
    if (!surface_ptr) {
        surface_ptr =
            loadSurfaceFile (this->getFilePath ("gfx", file_name));
        surfaces[file_name] = surface_ptr;
    }
    return surface_ptr;
}

SDL::Texture ResourceManager::texture (const std::string &file_name) const {
    TextureRes texture (this->textures[file_name]);
    SDL::Texture texture_ptr (texture.lock ());
    if (!texture_ptr) {
        texture_ptr =
            loadTextureFile (this->getFilePath ("gfx", file_name), this->renderer);
        textures[file_name] = texture_ptr;
    }
    return texture_ptr;
}

Music::Ptr ResourceManager::try_music (const std::string &file_name) const {
    try {
        return MusicMixer::New (this->music (file_name));
    } catch (std::invalid_argument &) {
        return MusicNull::New ();
    }
}

Sound::Ptr ResourceManager::try_sound (const std::string &file_name) const {
    try {
        return SoundMixer::New (this->sound (file_name));
    } catch (std::invalid_argument &) {
        return SoundNull::New ();
    }
}
