//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_CONTEXT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_CONTEXT_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <algorithm>
#include <cassert>
#include <map>
#include <list>
#include <string>
#include <SDL_render.h>
#include <boost/shared_ptr.hpp>
#include "SDL.hpp"
#include "Sprite.hpp"

namespace benzaiten {
    // Forward declarations.
    class ResourceManager;

    ///
    /// @class Context
    /// @brief Base class for game contexts
    ///
    /// A game context is an organizational unit that helps in breaking the
    /// game in logical parts, such as the main menu, a level, pause, etc.
    ///
    /// Game has at most a single active Context active at a time and the
    /// current context can be changed by passing a new context to
    /// Game::switchTo() or Game::fadeTo().
    ///
    /// Each context has a list of Sprite objects that updates and renders
    /// when appropriate as told by the game loop.
    ///
    class Context {
        public:
            /// A pointer to a Context instance.
            typedef boost::shared_ptr<Context> Ptr;

            ///
            /// @brief Constructor.
            ///
            Context ();

            ///
            /// @brief Virtual destructor.
            ///
            virtual ~Context () { }

            ///
            /// @brief Adds a new Sprite to the context.
            ///
            /// @param[in] sprite The sprite to add.
            ///
            /// @return The Sprite added.
            ///
            Sprite::Ptr add (const Sprite::Ptr &sprite);

            ///
            /// @brief Adds a simple Sprite with a texture.
            ///
            /// @param[in] file_name The name of the texture to use.
            /// @param[in] x The X position to set to the Sprite.
            /// @param[in] y The Y position to set to the Sprite.
            /// @param[in] layer The layer to set the Sprite to.
            ///
            /// @return The Sprite added.
            ///
            Sprite::Ptr addGraphic (const std::string &file_name,
                    double x = 0.0, double y = 0.0, int layer = 0);

            ///
            /// @brief Adds a simple Sprite with a texture.
            ///
            /// @param[in] texture The texture to set to the Sprite.
            /// @param[in] x The X position to set to the Sprite.
            /// @param[in] y The Y position to set to the Sprite.
            /// @param[in] layer The layer to set the Sprite to.
            ///
            /// @return The Sprite added.
            ///
            Sprite::Ptr addGraphic (const SDL::Texture &texture,
                    double x = 0.0, double y = 0.0, int layer = 0);

            ///
            /// @brief Adds a simple Sprite with a graphic.
            ///
            /// @param[in] graphic The graphic to set to the Sprite.
            /// @param[in] x The X position to set to the Sprite.
            /// @param[in] y The Y position to set to the Sprite.
            /// @param[in] layer The layer to set the Sprite to.
            ///
            /// @return The Sprite added.
            ///
            Sprite::Ptr addGraphic (const Graphic::Ptr &graphic,
                    double x = 0.0, double y = 0.0, int layer = 0);

            ///
            /// @brief Called when the Context is set active.
            ///
            virtual void begin () { }

            ///
            /// @brief Gets the first sprite of a given type.
            ///
            /// @param[in] type The type of the Sprite to get.
            ///
            /// @return The pointer to the first Sprite of type @p type or
            ///         @c NULL if there is no Sprite of @p type.
            ///
            Sprite *firstOfType (int type) const;

            ///
            /// @brief Removes the sprite from the Context.
            ///
            /// @param[in] sprite The pointer to the sprite to remove.
            ///
            void remove (Sprite *sprite);

            ///
            /// @brief Removes all sprites of a given type form the Context.
            ///
            /// @param[in] type The type of the sprites to remove.
            ///
            void removeOfType (int type);

            ///
            /// @brief Renders the Sprite in the Context to the display.
            ///
            /// This is usually called by the game loop and by default it
            /// renders all the Sprite objects that are visible in the Context.
            ///
            /// @param[in] renderer The renderer to use to render the context with.
            ///
            virtual void render (SDL_Renderer &renderer) const;

            ///
            /// @brief Updates all the Sprite objects in the Context.
            ///
            /// This function is usually called once per frame by the game
            /// loop to give the Context a chance to update its Sprite
            /// objects' logic.
            ///
            /// @param[in] elapsedTime The time that elapsed from the last
            ///            frame to the frame we are updating now, in
            ///            seconds.
            ///
            virtual void update (double elapsedTime);

        private:
            // To be able to update the lists.
            friend class Game;
            friend class Sprite;

            /// A list of Sprite objects.
            typedef std::list<Sprite *> SpriteList;

            /// A map of sprites.
            typedef std::map<int, Sprite *> SpriteMap;

            ///
            /// @brief Adds the Sprite to the list of sprites to render.
            ///
            /// @param[in] sprite The sprite to add to the render list.
            ///
            void addRender (Sprite *sprite);

            ///
            /// @brief Adds the Sprite to the list of types.
            ///
            /// If the Sprite has a type negative, then it is not added to
            /// any list.
            ///
            /// @param[in] sprite The sprite to add.
            ///
            void addType (Sprite *sprite);

            ///
            /// @brief Adds the Sprite to the list of sprites to update.
            ///
            /// @param[in] sprite The sprite to add to the update list.
            ///
            void addUpdate (const Sprite::Ptr &sprite);

            ///
            /// @brief Removes the Sprite from the list of sprites to render.
            ///
            /// @param[in] sprite The sprite to remove from the render list.
            ///
            void removeRender (Sprite *sprite);

            ///
            /// @brief Removes the Sprite from the list of types.
            ///
            /// @param[in] sprite The sprite to remove from the type list.
            ///
            void removeType (Sprite *sprite);

            ///
            /// @brief Removes the Sprite from the list of sprites to update.
            ///
            /// @param[in] sprite The sprite to remove from the update list.
            ///
            void removeUpdate (Sprite *sprite);

            ///
            /// @brief Updates the internal list of sprites.
            ///
            void updateLists ();

            /// The first Sprite that needs to be rendered.
            SpriteMap render_first;
            /// The maps of the list of sprites by list.
            SpriteMap type_first;
            /// The list of sprites to add.
            std::list<Sprite::Ptr> sprites_to_add;
            /// The list of sprite to remove.
            SpriteList sprites_to_remove;
            /// The first Sprite that needs to be updated.
            Sprite::Ptr update_first;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_CONTEXT_HPP
