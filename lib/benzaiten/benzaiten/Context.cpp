//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Context.hpp"
#include <cassert>
#include <boost/foreach.hpp>
#include "Game.hpp"
#include "ResourceManager.hpp"
#include "Sprite.hpp"
#include "Utils.hpp"
#include "graphics/Texture.hpp"

using namespace benzaiten;

Context::Context ():
    render_first (),
    type_first (),
    sprites_to_add (),
    sprites_to_remove (),
    update_first (static_cast<Sprite *> (0))
{
}

Sprite::Ptr Context::add (const Sprite::Ptr &sprite) {
    assert (sprite != 0 && "Tried to add a null Sprite");

    this->sprites_to_add.push_back (sprite);
    return sprite;
}

Sprite::Ptr Context::addGraphic (const std::string &file_name,
        double x, double y, int layer) {
    assert (!file_name.empty() && "Can't load an empty texture file name");
    return this->addGraphic (
            Game::resources().texture (file_name), x, y, layer);
}

Sprite::Ptr Context::addGraphic (const SDL::Texture &texture, double x,
        double y, int layer) {
    assert (texture != 0 && "Tried to add a graphic with a null Texture");
    return this->addGraphic (Texture::New (texture), x, y, layer);
}

Sprite::Ptr Context::addGraphic (const Graphic::Ptr &graphic, double x,
        double y, int layer) {
    assert (graphic != 0 && "Tried to add a null graphic");
    Sprite::Ptr sprite = Sprite::New (x, y, graphic);
    sprite->layer = layer;
    sprite->setActive (false);
    return this->add (sprite);
}

void Context::addRender (Sprite *sprite) {
    assert (sprite != 0 && "Tried to add a null Sprite to render");
    int layer = sprite->layer;
    if (this->render_first.find (layer) == this->render_first.end ()) {
        this->render_first[layer] = 0;
    } else if (this->render_first[layer] != 0) {
        this->render_first[layer]->render_prev = sprite;
    }
    sprite->render_next = this->render_first[layer];
    sprite->render_prev = 0;
    this->render_first[layer] = sprite;
}

void Context::addType (Sprite *sprite) {
    assert (sprite != 0 && "Tried to add a null Sprite to types.");
    int type = sprite->type;
    if (type > -1) {
        if (this->type_first.find (type) == this->type_first.end ()) {
            this->type_first[type] = 0;
        } else if (this->type_first[type] !=0) {
            this->type_first[type]->type_prev = sprite;
        }
        sprite->type_next = this->type_first[type];
        sprite->type_prev = 0;
        this->type_first[type] = sprite;
    }
}

void Context::addUpdate (const Sprite::Ptr &sprite) {
    assert (sprite != 0 && "Tried to add a null Sprite to update");

    if (this->update_first) {
        this->update_first->update_prev = sprite.get ();
    }
    sprite->update_next = this->update_first;
    sprite->update_prev = 0;
    this->update_first = sprite;
}

Sprite *Context::firstOfType (int type) const {
    std::map<int, Sprite *>::const_iterator first =
        this->type_first.find (type);
    if (first == this->type_first.end()) {
        return 0;
    }
    return first->second;
}

void Context::remove (Sprite *sprite) {
    assert (sprite != 0 && "Tried to remove a null Sprite");
    this->sprites_to_remove.push_back (sprite);
}

void Context::removeOfType (int type) {
    Sprite *sprite = this->firstOfType (type);
    while (sprite != 0) {
        this->remove (sprite);
        sprite = sprite->type_next;
    }
}

void Context::removeRender (Sprite *sprite) {
    assert (sprite != 0 && "Tried to remove a null Sprite from render");
    int layer = sprite->layer;
    assert (this->render_first.find (layer) != this->render_first.end () && "Removing a sprite from a non-existent layer");
    if (sprite->render_next != 0) {
        sprite->render_next->render_prev = sprite->render_prev;
    }
    if (sprite->render_prev == 0) {
        this->render_first[layer] = sprite->render_next;
    } else {
        sprite->render_prev->render_next = sprite->render_next;
    }
}

void Context::removeType (Sprite *sprite) {
    assert (sprite != 0 && "Tried to remove a null Sprite from type");
    int type = sprite->type;
    if (type > -1) {
        assert (this->type_first.find (type) != this->type_first.end () && "Removing a type not added");
        if (sprite->type_next != 0) {
            sprite->type_next->type_prev = sprite->type_prev;
        }
        if (sprite->type_prev == 0) {
            this->type_first[type] = sprite->type_next;
        } else {
            sprite->type_prev->type_next = sprite->type_next;
        }
    }
}

void Context::removeUpdate (Sprite *sprite) {
    assert (sprite != 0 && "Tried to remove a null Sprite from update");
    if (sprite->update_next != 0) {
        sprite->update_next->update_prev = sprite->update_prev;
    }
    if (sprite->update_prev == 0) {
        this->update_first = sprite->update_next;
    } else {
        sprite->update_prev->update_next = sprite->update_next;
    }
}

void Context::render (SDL_Renderer &renderer) const {
    BOOST_FOREACH (const SpriteMap::value_type &layer, this->render_first) {
        Sprite *current = layer.second;
        while (current != 0) {
            current->render (renderer);
            current = current->render_next;
        }
    }
}

void Context::update (double elapsed) {
    Sprite *current = this->update_first.get ();
    while (current != 0) {
        if (current->isActive ()) {
            current->update (elapsed);
            if (current->graphic != 0 && current->graphic->isActive ()) {
                current->graphic->update (elapsed);
            }
        }
        current = current->update_next.get ();
    }
}

void Context::updateLists () {
    BOOST_FOREACH (Sprite *sprite, this->sprites_to_remove) {
        if (sprite->context == 0) {
            // As the sprite belongs to no Context, first look
            // if we are told to add it.  If so, then there's no
            // point in adding it later and must be removed from the list.
            for(std::list<Sprite::Ptr>::iterator to_add = this->sprites_to_add.begin () ;
                    to_add != this->sprites_to_add.end () ; ++to_add) {
                if (to_add->get () == sprite) {
                    this->sprites_to_add.erase (to_add);
                    break;
                }
            }
            continue;
        }
        if (sprite->context != this) {
            continue;
        }
        sprite->context = 0;
        this->removeType (sprite);
        this->removeRender (sprite);
        this->removeUpdate (sprite);
    }
    this->sprites_to_remove.clear ();

    BOOST_FOREACH (Sprite::Ptr &sprite, this->sprites_to_add) {
        if (sprite->context == 0) {
            this->addUpdate (sprite);
            this->addRender (sprite.get ());
            this->addType (sprite.get ());
            sprite->context = this;
        }
    }
    this->sprites_to_add.clear ();
}
