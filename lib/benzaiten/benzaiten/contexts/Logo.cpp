//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Logo.hpp"
#include "../Game.hpp"
#include "../Utils.hpp"
#include "../graphics/Texture.hpp"

using namespace benzaiten::contexts;

Logo::Logo (const std::string &logo_file, const Context::Ptr &next,
        double display_time, double fade_time):
    alpha (SDL_ALPHA_OPAQUE),
    display_time (display_time),
    fade_in (0.0),
    fade_out (0.0),
    fade_time (fade_time),
    next (next)
{
    Texture::Ptr logo = Texture::New (Game::resources ().texture (logo_file));
    logo->centerOrigin ();
    Sprite::Ptr sprite = this->addGraphic (logo);
    sprite->moveTo (Game::screen().half_width, Game::screen().height / 3);
}

void Logo::render (SDL_Renderer &renderer) const {
    Context::render (renderer);

    SDL_Color color = Game::screen().color;
    SDL_SetRenderDrawColor (&renderer, color.r, color.g, color.b,
            this->alpha);
    SDL_BlendMode mode;
    SDL_GetRenderDrawBlendMode (&renderer, &mode);
    SDL_SetRenderDrawBlendMode (&renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderFillRect (&renderer, NULL);
    SDL_SetRenderDrawBlendMode (&renderer, mode);
}

void Logo::update (double elapsed) {
    Context::update (elapsed);

    if (this->fade_in < this->fade_time) {
        this->fade_in += elapsed;
        this->alpha = clamp (
                255.0 - 255.0 * this->fade_in / this->fade_time,
                0.0, 255.0);
    } else if (this->display_time > 0) {
        this->display_time -= elapsed;
        this->alpha = SDL_ALPHA_TRANSPARENT;
    } else if (this->fade_out < this->fade_time) {
        this->fade_out += elapsed;
        this->alpha = clamp (255.0 * this->fade_out / this->fade_time,
                0.0, 255.0);
    } else {
        Game::switchTo (this->next);
    }
}
