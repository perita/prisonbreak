@echo off
setlocal

set OLDDIR="%CD%"
set SOURCEDIR="%~dp0.%"
set BUILDDIR="%SOURCEDIR%\build"
set GENERATOR="MinGW Makefiles"
set MAKE=mingw32-make

:prepare_project
echo ================================
echo = Generating project files...
echo ================================
echo.
if exist %BUILDDIR% rmdir /Q /S %BUILDDIR%
if not 0 == %errorlevel% goto :error
if exist %BUILDDIR% goto :error
mkdir %BUILDDIR%
if not 0 == %errorlevel% goto :error
cd %BUILDDIR%
if not 0 == %errorlevel% goto :error
cmake -DCMAKE_CXX_FLAGS="-Wall" -DCMAKE_C_FLAGS="-Wall" -DCMAKE_BUILD_TYPE="Release" -G %GENERATOR% %SOURCEDIR%
if not 0 == %errorlevel% goto :error
echo.
echo.

echo ================================
echo = Building project...
echo ================================
echo.
cd %BUILDDIR%
if not 0 == %errorlevel% goto :error
%MAKE%
if not 0 == %errorlevel% goto :error
echo.
echo.

:isntaller
echo ================================
echo = Creating installation database...
echo ================================
echo.
cd %BUILDDIR%
if not 0 == %errorlevel% goto :error
%MAKE% msi_prisonbreak
if not 0 == %errorlevel% goto :error
copy *.msi %SOURCEDIR%
if not 0 == %errorlevel% goto :error
echo.
echo.

:done
cd %OLDDIR%
rmdir /Q /S %BUILDDIR%
echo.
echo.
echo Build success!!
echo.
if "%BITTEN_EXEC%" == "1" exit 0
exit /B 0

:error
cd %OLDDIR%
echo.
echo.
echo *** Error encountered while building!
echo.
if "%BITTEN_EXEC%" == "1" exit 1
exit /B 1
