//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LEVEL_HPP)
#define GEISHA_STUDIOS_LEVEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Context.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <boost/shared_ptr.hpp>

/**
 * @class Level
 * @brief A level of balls.
 *
 * This class is where the main portion of the game takes place.  A
 * level is a bunch of ball bouncing on the game's screen borders.
 *
 * The player can place a single explosion to the level by using the mouse
 * or the finger (which is the same from the point of view of the context.)
 *
 * If any ball gets in contact with the explosion, then the ball is removed
 * and a new explosion is placed on the ball's place.  Thus, players can
 * make chains of explosions to remove as many balls as the target is (or
 * more.)
 *
 * The level is won only if the number of balls removed are equal or bigger
 * then the level's target.
 */
class Level: public benzaiten::Context {
    public:
        /// A smart pointer to a Level.
        typedef boost::shared_ptr<Level> Ptr;

        ///
        /// @brief Creates a new Level in a smart pointer.
        ///
        /// @param[in] level The level number.  This is not used here,
        ///            but passed along to the next context when the
        ///            level is over.
        /// @param[in] balls The initial number of balls to place in
        ///            this level.
        /// @param[in] target The minimum number of balls to remove in
        ///            order to consider the level won.
        ///
        /// @return The smart pointer to the newly created Level.
        ///
        static Ptr New (unsigned int level, unsigned int balls,
                unsigned int target) {
            return Ptr (new Level (level, balls, target));
        }

        virtual void update (double elapsed);

    protected:
        ///
        /// @brief Constructor.
        ///
        /// @param[in] level The level number.  This is not used here,
        ///            but passed along to the next context when the
        ///            level is over.
        /// @param[in] balls The initial number of balls to place in
        ///            this level.
        /// @param[in] target The minimum number of balls to remove in
        ///            order to consider the level won.
        ///
        Level (unsigned int level, unsigned int balls, unsigned int target);

    private:
        /**
         * Updates the score's text.
         *
         * Each time a ball is removed from the Level, the score text
         * should be changed to reflect the new status.  This simply
         * sets sets the score's text as "removed/target".
         */
        void updateScore ();

        /// Whether the player placed the first explosion.
        bool first_explosion;
        /// The level number.
        unsigned int level;
        /// The number of balls removed.
        unsigned int removed;
        /// The score's text.
        benzaiten::Text::Ptr score;
        /// The minimum number of balls to remove to succeed.
        unsigned int target;
};

#endif // !GEISHA_STUDIOS_LEVEL_HPP
