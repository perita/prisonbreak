//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "Game.hpp"
#include <cassert>
#include <stdexcept>
#include <SDL.h>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>

#if defined (GP2X)
#include <unistd.h> // for chdir and execl
#endif // GP2X

#if defined (EMSCRIPTEN)
#include <emscripten.h>
#include <emscripten/html5.h>

#define EMSCRIPTEN_STATIC static
#else
#define EMSCRIPTEN_STATIC
#endif // EMSCRIPTEN


using namespace benzaiten;

namespace {
    ///
    /// @class SetAttr
    /// @brief RAII class to set an attribute pointer.
    ///
    /// It sets the attribute pointer to the value passed in the constrctor
    /// and resets the attriubte to 0 in the destructor.
    ///
    template <typename T> struct SetAttr: boost::noncopyable {
        SetAttr (Game *game, T *Game::* attribute, T *value)
            : game(game)
            , attribute (attribute)
        {
            game->*attribute = value;
        }

        ~SetAttr () {
            game->*attribute = 0;
        }

        Game *game;
        T *Game::* attribute;
    };

    ///
    /// @class SetVar
    /// @brief RAII class to set a variable pointer.
    ///
    /// It sets the variable pointer to the value passed in the constrctor
    /// and resets the variable to 0 in the destructor.
    ///
    template <typename T> struct SetVar: boost::noncopyable {
        SetVar (T **variable, T *value)
            : variable (variable)
        {
            *(variable) = value;
        }

        ~SetVar () {
            *(variable) = 0;
        }

        T **variable;
    };
}

// This is *the* game instance.
Game *Game::the_game = 0;

Game::Game (int width, int height, const std::string &title,
        const std::string &game_id, bool enable_audio, double fps)
    : active_context ()
    , enable_audio (enable_audio)
    , game_camera (0.0, 0.0)
    , game_id (game_id)
    , game_screen (0)
    , input_manager (0)
    , previous_context ()
    , resource_manager (0)
    , screen_fill_policy (Screen::FILL_NONE)
    , screen_height (height)
    , screen_title (title)
    , screen_width (width)
    , frameRate (fps)
{
}

Game::~Game () {
#if defined (GP2X)
    chdir ("/usr/gp2x");
    execl ("/usr/gp2x/gp2xmenu", "/usr/gp2x/gp2xmenu", NULL);
#endif // GP2X
#if defined (ANDROID)
    exit (0);
#endif // ANDROID
}

void Game::operator() () {
    assert (the_game == 0 && "There is another instance of Game running!");

    SdlInit sdl (this->enable_audio);
    EMSCRIPTEN_STATIC Screen screen (this->screen_width, this->screen_height,
            this->screen_title, this->screen_fill_policy);
    EMSCRIPTEN_STATIC ResourceManager resources (this->game_id, *(screen.renderer));
    EMSCRIPTEN_STATIC InputManager input_manager_inst;

    EMSCRIPTEN_STATIC boost::scoped_ptr<Audio> audio;
    if (this->enable_audio) {
        audio.reset (new Audio (44100, Audio::S16SYS, Audio::Stereo, 2048));
    }

#if defined (GP2X) || defined (A320) || defined (PANDORA)
    // This must be called *after* the call to SDL_SetVideoMode.
    SDL_ShowCursor (0);
#endif // GP2X || A320 || PANDORA

    SetAttr<Screen> screen_ptr (this, &Game::game_screen, &screen);
    SetAttr<ResourceManager> resources_ptr (this, &Game::resource_manager,
            &resources);
    SetAttr<InputManager> input_manager_ptr (this, &Game::input_manager,
            &input_manager_inst);
    SetVar<Game> game_ptr (&Game::the_game, this);
    this->init ();

    this->game_camera = Point2D<double> (0.0, 0.0);
#if defined(EMSCRIPTEN)
    emscripten_set_main_loop_arg(&Game::runOnce, this, 0, 1);
#else
    while (this->active_context) {
	    this->runOnce();
    }
#endif // !EMSCRIPTEN

    this->cleanUp ();
}

void Game::runOnce() {
    if (!this->active_context) {
    	return;
    }
    Context::Ptr context (this->active_context);
    if (context != this->previous_context) {
        context->begin ();
    }
    this->previous_context = context;

    frameRate.update ();
    this->processEvents (*input_manager);
    context->updateLists ();
    context->update (frameRate.getElapsedTime ());

    SDL_Renderer *renderer = game_screen->renderer.get ();
    game_screen->beginRender ();
    context->render (*renderer);
    game_screen->endRender ();
}

void Game::runOnce(void *game) {
    reinterpret_cast<Game *>(game)->runOnce();
}

void Game::processEvents (InputManager &input_manager) {
    input_manager.update ();

#if !defined (EMSCRIPTEN)
    SDL_Touch *touch;
#endif // !EMSCRIPTEN
    SDL_Event event;
    while (SDL_PollEvent (&event)) {
        switch (event.type) {
#if !defined (EMSCRIPTEN)
            case SDL_FINGERDOWN:
                touch = SDL_GetTouch (event.tfinger.touchId);
                input_manager.mouse_down = true;
                input_manager.mouse_pressed = true;
                input_manager.mouse_x = event.tfinger.x *
                    (double(this->game_screen->width) / touch->xres);
                input_manager.mouse_y = event.tfinger.y *
                    (double(this->game_screen->height) / touch->yres);
                break;

            case SDL_FINGERMOTION:
                touch = SDL_GetTouch (event.tfinger.touchId);
                input_manager.mouse_x = event.tfinger.x *
                    (double(this->game_screen->width) / touch->xres);
                input_manager.mouse_y = event.tfinger.y *
                    (double(this->game_screen->height) / touch->yres);
                break;

            case SDL_FINGERUP:
                touch = SDL_GetTouch (event.tfinger.touchId);
                input_manager.mouse_down = false;
                input_manager.mouse_released = true;
                input_manager.mouse_x = event.tfinger.x *
                    (double(this->game_screen->width) / touch->xres);
                input_manager.mouse_y = event.tfinger.y *
                    (double(this->game_screen->height) / touch->yres);
                break;

            case SDL_JOYBUTTONDOWN:
            case SDL_JOYBUTTONUP:
                input_manager.handleButton (event.jbutton);
                break;

            case SDL_JOYAXISMOTION:
                input_manager.handleAxis (event.jaxis);
                break;

            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == 1) {
                    input_manager.mouse_down = true;
                    input_manager.mouse_pressed = true;
                }
                break;
#endif // !EMSCRIPTEN

            case SDL_MOUSEBUTTONUP:
                if (event.button.button == 1) {
                    input_manager.mouse_down = false;
                    input_manager.mouse_released = true;
                }
                break;

            case SDL_MOUSEMOTION:
                input_manager.mouse_x = event.motion.x / Game::screen ().scale;
                input_manager.mouse_y = event.motion.y / Game::screen ().scale;
                break;

            case SDL_QUIT:
                Game::quit ();
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                input_manager.handleKeyboard (event.key);
                break;
        }
    }
}

void Game::quit () {
    Game::switchTo (Context::Ptr ());
}

void Game::switchTo (Context::Ptr context) {
    assert (the_game != 0);

    the_game->previous_context = the_game->active_context;
    the_game->active_context = context;
}

Game::SdlInit::SdlInit (bool audio) {
    Uint32 subsystems = SDL_INIT_VIDEO | SDL_INIT_JOYSTICK;
    if (audio) {
        subsystems |= SDL_INIT_AUDIO;
    }
    if ( SDL_Init (subsystems) < 0 ) {
        throw std::runtime_error (SDL_GetError ());
    }
}

Game::SdlInit::~SdlInit () {
    SDL_Quit ();
}
