//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_TEXTURE_HPP)
#define GEISHA_STUDIOS_TEXTURE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <SDL_render.h>
#include <boost/shared_ptr.hpp>
#include "../Graphic.hpp"
#include "../Point2D.hpp"
#include "../SDL.hpp"

namespace benzaiten {
    ///
    /// @class Texture
    /// @brief Shows a static texture.
    ///
    class Texture: public Graphic {
        public:
            /// A pointer to a texture.
            typedef boost::shared_ptr<Texture> Ptr;

            ///
            /// @brief Creates an empty Texture.
            ///
            static Ptr New () { return Ptr (new Texture ()); }

            ///
            /// @brief Creates the texture from the given file.
            ///
            /// @param[in] file_name The file name of the Texture to load
            ///
            static Ptr New (const std::string &file_name) {
                return Ptr (new Texture (file_name));
            }

            ///
            /// @brief Creates the texture with the given data.
            ///
            /// @param[in] data The data for the texture.
            ///
            static Ptr New (const SDL::Texture &data) {
                return Ptr (new Texture (data));
            }

            ///
            /// @brief Sets the origin in the center of the texture.
            ///
            virtual void centerOrigin ();

            virtual void render (SDL_Renderer &render, int x, int y) const;

            ///
            /// @brief Sets the clip region.
            ///
            /// The clip region is the part of the texture that will be rendered
            /// on the screen.
            ///
            /// @param[in] x The X starting point of the region.
            /// @param[in] y The Y starting point of the region.
            /// @param[in] width The region's width.
            /// @param[in] height The region's height.
            ///
            void setClip (int x, int y, int width, int height);

        protected:
            ///
            /// @brief Creates an empty Texture.
            ///
            Texture ();

            ///
            /// @brief Creates the texture with the given data.
            ///
            /// @param[in] data The data for the texture.
            ///
            Texture (const SDL::Texture &data);

            ///
            /// @brief Creates the texture from the given file.
            ///
            /// @param[in] file_name The file name of the Texture to load
            ///
            Texture (const std::string &file_name);

            ///
            /// @brief Gets the full texture's size.
            ///
            /// @return The texture's full size; without clipping.
            ///
            SDL_Rect getFullSize () const { return this->size; }

            ///
            /// @brief Sets the texture to use on render.
            ///
            /// @param[in] texture The new texture to set.
            ///
            void setTexture (const SDL::Texture &texture);

        private:
            /// The clipping area.
            SDL_Rect clip;
            /// The texture data.
            SDL::Texture data;
            /// The texture's render origin.
            Point2D<double> origin;
            /// The texture's size.
            SDL_Rect size;
    };
}

#endif // !GEISHA_STUDIOS_TEXTURE_HPP
