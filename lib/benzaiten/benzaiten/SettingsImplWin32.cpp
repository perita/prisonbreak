//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "SettingsImplWin32.hpp"

using namespace benzaiten;

SettingsImplWin32::SettingsImplWin32 (const std::string &appName):
    SettingsBase::Impl (),
    registrySubKey_ (std::string ("Software\\Geisha Studios\\") + appName + "\\")
{
}

int
SettingsImplWin32::getInteger (const std::string &section,
    const std::string &name, int defaultValue) const
{
    int value = defaultValue;
    std::string path (registrySubKey_ + section);

    HKEY key;
    if ( ERROR_SUCCESS == RegOpenKeyEx (HKEY_CURRENT_USER, path.c_str (),
                                0, KEY_READ, &key) )
    {
        DWORD valueSize = sizeof (value);
        RegQueryValueEx (key, name.c_str (), NULL, NULL,
            reinterpret_cast<BYTE *> (&value), &valueSize);
        RegCloseKey (key);
    }

    return value;
}

std::string
SettingsImplWin32::getString(const std::string &section,
    const std::string &name, const std::string &defaultValue) const
{
    std::string value = defaultValue;
    std::string path(registrySubKey_ + section);

    HKEY key;
    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_CURRENT_USER, path.c_str(),
                               0, KEY_READ, &key))
    {
        char registryValue[512];
        DWORD valueSize = sizeof(registryValue);
        if (ERROR_SUCCESS == RegQueryValueEx(key, name.c_str(), NULL, NULL,
            reinterpret_cast<BYTE *>(&registryValue), &valueSize))
        {
            registryValue[valueSize - 1] = '\0';
            value = std::string(registryValue);
        }
        RegCloseKey(key);
    }

    return value;
}

void
SettingsImplWin32::setInteger (const std::string &section,
    const std::string &name, int value)
{
    writeValue(section, name, REG_DWORD, reinterpret_cast<BYTE *>(&value),
        sizeof(value));
}

void
SettingsImplWin32::setString(const std::string &section,
    const std::string &name, const std::string &value)
{
    writeValue(section, name, REG_SZ,
        reinterpret_cast<BYTE *>(const_cast<char *>(value.c_str())),
        value.size());
}

void
SettingsImplWin32::writeValue(const std::string &section, const std::string &name,
    DWORD type, BYTE *value, DWORD valueSize)
{
    std::string path(registrySubKey_ + section);

    HKEY key;
    if (ERROR_SUCCESS == RegCreateKeyEx(HKEY_CURRENT_USER, path.c_str(),
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE,
        NULL, &key, NULL))
    {
        RegSetValueEx(key, name.c_str(), 0, type, value, valueSize);
        RegCloseKey(key);
    }
}
