//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <cstddef>
#include <map>
#include <stdexcept>
#include <string>
#include <SDL_render.h>
#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include "MusicMixer.hpp"
#include "SDL.hpp"
#include "SoundMixer.hpp"
#include "graphics/TextureAtlas.hpp"

namespace benzaiten
{
    ///
    /// @class ResourceManager
    /// @brief Manages the resources needed by other classes (surface, etc.)
    ///
    class ResourceManager: boost::noncopyable
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// Gets the more addequate directory from where to get the
            /// resources later based on the platform.
            ///
            /// @param[in] game_id The name of the directory to use to fetch
            ///            the resources from.
            /// @param[inout] renderer The reference to the renderer to use to
            ///               load textures.
            ///
            ResourceManager (const std::string &game_id,
                    SDL_Renderer &renderer);

            /// @brief Gets the path to a resource file in a directory.
            ///
            /// @param[in] directory The directory where the file is located at.
            /// @param[in] file_name The name of the file to get its path in
            ///            the resource @p directory.
            ///
            /// @return The path to the resource files.
            ///
            std::string getFilePath (const std::string &directory,
                    const std::string &file_name) const;

            ///
            /// @brief Loads a TextureAtlas from an XML definition.
            ///
            /// @param[in] file_name The file name of the XML definition to
            ///            load the TextureAtlas from.
            ///
            /// @return The TextureAtlas loaded.
            ///
            /// @throw std::invalid_argument If the resource couldn't be
            ///        loaded.
            ///
            TextureAtlas::Ptr atlas (const std::string &file_name) const;

            ///
            /// @brief Loads a music file.
            ///
            /// @param[in] file_name The file name of the music resource to
            ///            load.
            ///
            /// @return The music loaded.
            ///
            /// @throw std::invalid_argument If the resource couldn't be
            ///        loaded.
            ///
            SDL::Music music (const std::string &file_name) const;

            ///
            /// @brief Loads a sound file.
            ///
            /// @param[in] fileName The file name of the sound resource to
            ///            load.
            ///
            /// @return The loaded sound.
            ///
            /// @throw std::invalid_argument If the resource couldn't be
            ///        loaded.
            ///
            SDL::Sound sound (const std::string &file_name) const;

            ///
            /// @brief Loads a surface.
            ///
            /// @param[in] file_name The file name of the surface to load.
            ///
            /// @return The loaded SDL_Surface.
            ///
            /// @throw std::invalid_argument If the resource couldn't be
            ///        loaded.
            ///
            SDL::Surface surface (const std::string &file_name) const;

            ///
            /// @brief Loads a texture.
            ///
            /// @param[in] file_name The file name of the texture to load.
            ///
            /// @return The loaded SDL_Texture.
            ///
            /// @throw std::invalid_argument If the resource couldn't be
            ///        loaded.
            ///
            SDL::Texture texture (const std::string &file_name) const;

            ///
            /// @brief Try to load a music file.
            ///
            /// @param[in] file_name The file name of the music resource to try
            ///            to load.
            ///
            /// @return A pointer to an actual implementation of a sound
            ///         object if @p fileName could be loaded.  A pointer to an
            ///         empty implementation otherwise.
            ///
            Music::Ptr try_music (const std::string &file_name) const;

            ///
            /// @brief Try to load a sound file.
            ///
            /// @param[in] file_name The file name of the source resource to
            ///            try to load.
            ///
            /// @return A pointer to an actual implementation of a sound
            ///         object if @p fileName could be loaded.  A pointer to an
            ///         empty implementation otherwise.
            ///
            Sound::Ptr try_sound (const std::string &file_name) const;

        private:
            /// A music resource.
            typedef boost::weak_ptr<Mix_Music> MusicRes;
            /// A sound resource.
            typedef boost::weak_ptr<Mix_Chunk> SoundRes;
            /// A surface.
            typedef boost::weak_ptr<SDL_Surface> SurfaceRes;
            /// A texture.
            typedef boost::weak_ptr<SDL_Texture> TextureRes;

            ///
            /// @brief Finds where the resources' path is.
            ///
            /// Using known information about the platform, finds which is
            /// the best suited resources path.
            ///
            /// @param game_id The ID of the application to use as the
            ///        resources' folder name.
            ///
            /// @return The best resources path, relative to the current
            ///         working directory or absolute, where the resources
            ///         are located.
            ///
            static std::string findResourcesPath (const std::string &game_id);

            ///
            /// @brief Join two path depending on the platform.
            ///
            /// @param[in] path0 The first path to join.
            /// @param[in] path1 The second path to join.
            ///
            /// @return "path0/path1" or "path0\\path1" depending on the
            ///         platform.
            ///
            static std::string joinPath (const std::string &path0,
                    const std::string &path1);

            ///
            /// @brief Loads a surface file.
            ///
            /// @param[in] file_name The name of the file to load.
            ///
            /// @return The smart pointer to the loaded surface.
            ///
            /// @throw std::invalid_argument If the graphic could not be
            ///        loaded.
            ///
            static SDL::Surface loadSurfaceFile (const std::string &file_name);

            ///
            /// @brief Loads a texture file.
            ///
            /// @param[in] file_name The name of the file to load.
            /// @param[inout] renderer The renderer to use to load the texture.
            ///
            /// @return The smart pointer to the loaded texture.
            ///
            /// @throw std::invalid_argument If the graphic could not be
            ///        loaded.
            ///
            static SDL::Texture loadTextureFile (const std::string &file_name,
                    SDL_Renderer &renderer);

            ///
            /// @brief Loads a TextureAtlas file.
            ///
            /// @param[in] file_name The name of the file to load.
            ///
            /// @return The smart pointer to the loaded TextureAtlas.
            ///
            /// @throw std::invalid_argument If the TextureAtlas could not be
            ///        loaded.
            ///
            TextureAtlas::Ptr loadTextureAtlasFile (
                    const std::string &ifle_name) const;

            ///
            /// @brief Loads a music file.
            ///
            /// @param[in] file_name The name of the file to load.
            ///
            /// @return The smart pointer to the loaded music resource.
            ///
            /// @throw std::invalid_argument If the music could not be loaded.
            ///
            static SDL::Music loadMusicFile (const std::string &file_name);

            ///
            /// @brief Loads a sound file.
            ///
            /// @param[in] file_name The name of the file to load.
            ///
            /// @return The smart pointer to the loaded sound resource.
            ///
            /// @throw std::invalid_argument If the sound could not be loaded.
            ///
            static SDL::Sound loadSoundFile (const std::string &file_name);

            /// The loaded musics.
            mutable std::map<std::string, MusicRes> musics;
            /// The renderer to use to load textures.
            SDL_Renderer &renderer;
            /// The path there the resources are.
            std::string resources_path;
            /// The loaded sounds.
            mutable std::map<std::string, SoundRes> sounds;
            /// The loaded surfaces.
            mutable std::map<std::string, SurfaceRes> surfaces;
            /// The loaded textures.
            mutable std::map<std::string, TextureRes> textures;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP
