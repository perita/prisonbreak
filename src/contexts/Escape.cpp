//
// Prison Break - Escape from prison.
// Copyright (C) 2009, 2010, 2012 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Escape.hpp"
#include <cassert>
#include <ctime>
#include <benzaiten/Game.hpp>
#include <benzaiten/SpriteTypeIterator.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include "../graphics/Grid.hpp"
#include "../sprites/Halo.hpp"
#include "../sprites/Guard.hpp"

using namespace prisonbreak;

namespace
{
    const double INITIAL_SPAWN_PROBABILITY = 0.999;
    const double VSPEED = 100.0;
}

Escape::Escape (const benzaiten::BitmapFont &font)
    : Context ()
    , alarm (benzaiten::Game::resources ().try_sound ("alarm.ogg"))
    , grid ()
    , meters (0.0)
    , meters_text (benzaiten::Text::New (font, ""))
    , music (benzaiten::Game::resources().try_music ("bgm.ogg"))
    , player (Player::New ())
    // The value *must* be unsigned.
    , random_generator (static_cast<unsigned int>(std::time(0)))
    , random_direction (random_generator, boost::uniform_int<>(0, 1))
    , random_position (random_generator, boost::uniform_int<>(0,
                benzaiten::Game::screen().width))
    , random_probability (random_generator, boost::uniform_real<>(0.0, 1.0))
    , random_speed (random_generator, boost::uniform_real<double>(1.0, 4.0))
    , reset_music (false)
    , spawn_probability (INITIAL_SPAWN_PROBABILITY)
    , start_text (benzaiten::Text::New (font, "TOUCH TO PLAY"))
    , volume (Volume::New (benzaiten::Game::screen ().width - 24,
                benzaiten::Game::screen ().height - 24, false))
    , wait_start (true)
#if defined (ANDROID)
    , arrow_left ()
    , arrow_right ()
    , input_text (benzaiten::Text::New (font, "TOUCH"))
    , use_accelerator (false)
#endif // ANDROID
{
    using benzaiten::Game;

#if defined (ANDROID)
    this->player->useController (this->use_accelerator);
    this->player->useMouse (!this->use_accelerator);
    this->input_text->centerOrigin ();
    this->addGraphic (this->input_text, Game::screen ().half_width,
            Game::screen().half_height / 2, 2);
    this->arrow_left = this->addGraphic (
                Game::resources ().texture ("arrow_left.tga"),
            Game::screen ().half_width - 85,
            Game::screen ().half_height / 2 - 5, 2);
    this->arrow_left->setHitbox (17, 19, 48, 48);
    this->arrow_right = this->addGraphic (
                Game::resources ().texture ("arrow_right.tga"),
            Game::screen ().half_width + 70,
            Game::screen ().half_height / 2 - 5, 2);
    this->arrow_right->setHitbox (17, 19, 48, 48);
#else // !ANDROID
    start_text->setText ("PRESS ANY KEY TO PLAY");
#endif // ANDROID

    this->grid = Grid::New (VSPEED);
    this->grid->setActive (false);
    this->addGraphic (grid)->setActive (true);
    this->add(Halo::New (this->player));

    this->player->setLayer (1);
    this->player->setActive (false);
    this->player->moveTo (
            Game::screen ().half_width, Game::screen ().height * 2 / 3);
    this->add (this->player);

    this->meters_text->centerOrigin ();
    this->addGraphic (this->meters_text, Game::screen ().half_width,
            font.height () * 2, 2);
    this->meters_text->setVisible (false);

    this->start_text->centerOrigin ();
    this->addGraphic (this->start_text, Game::screen ().half_width,
            Game::screen ().half_height, 2);

    this->volume->setLayer (2);
    this->add (this->volume);
    if (!this->volume->isMute ()) {
        this->music->play ();
    }
}

void Escape::reset () {
    using benzaiten::Game;

#if defined (ANDROID)
    this->arrow_left->getGraphic ()->setVisible (false);
    this->arrow_right->getGraphic ()->setVisible (false);
    this->input_text->setVisible (false);
#endif // ANDROID

    this->removeOfType (Types::GUARD);
    this->meters = 0.0;
    this->spawn_probability = INITIAL_SPAWN_PROBABILITY;
    this->grid->setActive (true);
    this->player->setActive (true);
    this->player->moveTo (
            Game::screen ().half_width, Game::screen ().height * 2 / 3);
    this->meters_text->setVisible (false);
    this->start_text->setVisible (false);
    this->wait_start = false;
    if (this->reset_music) {
        if (!this->volume->isMute ()) {
            this->music->play ();
        }
        this->reset_music = false;
    }
}

void Escape::stop () {
    if (!this->volume->isMute ()) {
        this->alarm->play ();
    }
    this->wait_start = true;
    this->start_text->setVisible (true);
    this->meters_text->setText (
            boost::str(boost::format ("YOU ESCAPED %1% METERS") %
                int(this->meters)));
    this->meters_text->setVisible (true);
    this->player->setActive (false);
    this->grid->setActive (false);
    this->reset_music = true;

#if defined (ANDROID)
    this->arrow_left->getGraphic ()->setVisible (true);
    this->arrow_right->getGraphic ()->setVisible (true);
    this->input_text->setVisible (true);
#endif // ANDROID
}

void Escape::toggleVolume () {
    this->volume->toggle ();
    if (this->volume->isMute ()) {
        this->music->pause ();
    } else {
        this->music->resume ();
    }
}

void Escape::update (double elapsed) {
    using benzaiten::Game;
    using benzaiten::spritesOfType;

    Context::update (elapsed);

    if (Game::input ().mouse_pressed) {
        benzaiten::Point2D<double> point (
                Game::input ().mouse_x, Game::input ().mouse_y);
        if (this->volume->collides (point)) {
            Game::input ().mouse_pressed = false;
            this->toggleVolume ();
        }
    }

    bool mute_key_pressed = false;
    if (Game::input ().pressed (Actions::MUTE)) {
        mute_key_pressed = true;
        this->toggleVolume ();
    }

    if (this->wait_start) {
#if !defined (ANDROID)
        if ((Game::input ().pressed (benzaiten::Action::ANY) &&
                    !mute_key_pressed)) {
            this->reset ();
        }
#endif // !ANDROID

        if (Game::input ().mouse_pressed) {
        benzaiten::Point2D<double> point (
                Game::input ().mouse_x, Game::input ().mouse_y);
#if defined (ANDROID)
            if (this->arrow_left->collides (point) ||
                    this->arrow_right->collides (point)) {
                this->use_accelerator = !this->use_accelerator;
                this->player->useController (this->use_accelerator);
                this->player->useMouse (!this->use_accelerator);
                this->input_text->setText (
                        this->use_accelerator ? "ACCELERATOR" : "TOUCH");
            }
            else
#endif // ANDROID
            {
                this->reset ();
            }
        }
    } else {
        this->meters += VSPEED * elapsed / 10.0;
        if (this->random_probability () > this->spawn_probability) {
            this->add (Guard::New (
                        this->random_position (),
                        this->random_direction () == 0 ? Guard::LEFT : Guard::RIGHT,
                        benzaiten::makePoint2D(
                            this->random_speed () * 70.0, VSPEED)));
        }
        this->spawn_probability -= 0.000005 * VSPEED * elapsed;
        BOOST_FOREACH (benzaiten::Sprite &guard,
                spritesOfType (this->firstOfType (Types::GUARD))) {
            if (this->player->distanceSquaredTo (guard) < (20 * 20)) {
                this->stop ();
                break;
            }
        }
    }

    if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
        Game::quit ();
    }
}
